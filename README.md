# Screen Traversal Genetic Algorithm
This project uses a genetic algorithm to train a neural network that navigates through a drawn map from the top left of the image to the bottom right of the image. The neural networks are called Individuals. Each individual can accelerate in the positive and negative x and y directions as well as do nothing; each one of these actions is considered a move. It is considered living if it has not 1) ran into a wall, 2) ran out moves, 3) has not left the first 10x10 pixels within 100 moves. The simulation is run in generations, which each have its own population of individuals. After each generation, the connections within the neural network or treated as the chromosomes in the genetic algorithm. They get selected, crossover-ed, and mutated, and a new generation is simulated.<br/>
This was the planning UML. Not updated or complete, but this was crucial to planning.<br/>

![alt-text](src/main/resources/static/images/StartingUML.jpg "Rough Draft UML")

***
## Individuals
Individuals are the individual racers in this project. Each individual contains a Neural Network and a fitness evaluation function
<br />

### Neural Network
Each individual contains a neural network. However, it is planned to explore the effects of different FNN structures and types in reference to performance and generalization, which means that there shouldn't be a set FNN structure. Individuals have a set of inputs: they have 16 distances to walls in different directions, the x and y position, velocity, and acceleration, and speed and direction. Each Neural Network can contain any of these inputs, and is not required to use all of them.

<p style="text-align:center">
<img src="src/main/resources/static/images/IndividualImage.png" width="500"/>
<br />
<p/>

After each input layer, there's some arbitrary number of hidden layers with an arbitrary amount of nodes; it's finished by an output layer that describes the action space of the individual. Two example output spaces are below. The first one can change the speed and direction of travel. The second changes the acceleration in the positive and negative x and y directions.

<p style="text-align:center">
<br/>
<img src="src/main/resources/static/images/IndividualActionSpace1.png" width="200"/>
<img src="src/main/resources/static/images/IndividualActionSpace2.png" width="200"/>
<br/>
<p/>

### Fitness Function
The fitness function scores each individual; therefor, it is a score that represents how well the neural network performed. Currently, it is rewarding distance from the start position and detriments any action taken, so it is encouraged to get far away as fast as possible. It also sets a minimum value of 0
```java
    public void calcFitness(){
        fitness = 10*Math.sqrt(Math.pow(x,2) + Math.pow(y,2)) - numMoves * .3;
        if (fitness < 0) { fitness = 0; }
    }
```

### Relationship to Genetic Algorithm
Genetic Algorithms contain genes to identify individuals within a population. They use those genes to change the individuals during reproduction/crossover and mutations. Individuals in this project use the Connection weights and neuron biases as the chromosomes within the genes. 


***
## Genetic Algorithm

### Selection
There are two selection processes in the code. <br />
Roulette Selection: <br />
This chooses individuals with respect to their fitness / total fitness. It randomly chooses individuals with each individual having (fitness / total fitness) chance. It is commonly thought of as a pie chart with each individual having a slice. This has pretty good exploration vs exploitation. The main concern is that the best performers are not guaranteed to make it to the next generation, which means it is possible to lose progress.<br />
![alt-text](src/main/resources/static/images/RouletteWheelSelection.png "Roulette Wheel Selection")
<br />
<br />
Code:

```java
public ArrayList<com.gaenvironmentstudy.app.Individual> rouletteSelect(){
        ArrayList<com.gaenvironmentstudy.app.Individual> selected = new ArrayList<com.gaenvironmentstudy.app.Individual>();
        double totalFitness = population.getTotalFitness();
        int cumulative;

        for (int j = 0; j < NUM_PARENTS; j++){
            double pick = rand.nextDouble() * totalFitness;
            cumulative = 0;
            for (com.gaenvironmentstudy.app.Individual i : population.getPopulation()){
                cumulative += i.getFitness();
                if (cumulative > pick){
                    i.setColor(Color.blue);
                    selected.add(i);
                    break;
                }
            }
        }
        return selected;
    }
```
<br />
<br />
Elitist Selection: <br/>
This selection simply selects the best "NUM_PARENTS" amount, which means it strongly favors exploitation because it does not select children that are anything less than the current best.
<br/>
Code:

```java
public ArrayList<com.gaenvironmentstudy.app.Individual> elitistSelect(){
        return population.getFittest(NUM_PARENTS);
    }
```


### Crossover
There are two types of crossover written in the code. Crossover takes two parents to produce two children. The way the genes are assigned or manipulated determines the type of crossover. <br/>
#### Uniform Binary Crossover:
This one uses a random binary operator that assigns either the 1st parents gene to the 1st child and the 2nd parent's to the 2nd child's, or it assigns the 2nd parent's gene to the 1st child and the 1st parent's to the 2nd child's. Whichever gene goes to the first child, the second child gets the other one. This is repeated for every gene in the chromosome. The children are inverses with regard to parent chromosomes. <br />


#### Single Point Crossover:
This one chooses a random point within the range of the chromosomes and every gene above that point is assigned from one parent, while everything below is from the other parent. The children are the inverse selections of each other.


### Mutate
Every child chromosome's genes have a small chance of mutating. That chance is the probability of mutation, and if it is mutated, that single gene is adjusted by a small gaussian amount that is scaled by the mutation scale.

***
## Data Collection and Analysis
Directory: src/main/java/DataHandling/
### Summary (TLDR)
Shell Script:
```shell
cd src/main/java/DataHandling/
# How to run GeneticAlgorithm.jar
java -jar GeneticAlgorithm.jar <nn structure> <#generations> <#individuals per generation> <#parents> <#elitists> <Mutation Scale> <Mutation Rate> <Path to Map.png>

# Creates data for 8 neural networks on 9 maps
./MassTesting.sh
```
GenerateResults.java on the parent directory: This makes the results of every nn on every map <br />
Update into Excel and analyze: Compare each nn of each structure, and then compare each structure to each map.<br />
The average of the normalized scores on every map is the generalization of a FNN trained on a specific map. Taking the average across different training maps will give a score that represents how well a particular structure of FNN can generalize. From there compare to other structures and make specific and appropriate conclusions.<br />

### Collection
For collection, in this directory, there is GeneticAlgorithm.jar that is an executable package that allows for command line interaction. It is run through the command:<br />
```shell
cd src/main/java/DataHandling/
java -jar GeneticAlgorithm.jar <nn structure> <#generations> <#individuals per generation> <#parents> <#elitists> <Mutation Scale> <Mutation Rate> <Path to Map.png>
```
When this is ran, it outputs two files: DB_Data.csv and FCNN.csv. These files hold data regarding the simulation and the resulting neural network respectively. This can be ran one at a time, or scripted to run multiple times.<br />
```shell
cd src/main/java/DataHandling/
./MassTesting.sh
```
**MassTesting.sh**<br /> 
This runs multiple neural networks on multiple maps. The neural networks are specified in the script, and the maps are in the local directory "Maps/". After each run of GeneticAlgorithm.jar, it copies the nn into a folder named after that neural network and into a file with the name of the map it was trained on. The DB_Data.csv is copied into a folder called DB_Data and renamed to the "nn"-"mapName".csv The neural network data is later used for analysis, and the DB_Data is used for database storage in a separate project not attached to this. <br />
<br />
**GenerateResults.java** <br />
After the neural networks have been trained, GenerateResults will trial every neural network on each map, normalize it to the neural network that was trained on that map, and then record it in a csv file titled results.csv. Each nn directory has a results.csv; These are then imported into Excel for analysis.

### Analysis
![alt-text](src/main/resources/static/images/ExcelOneNeuralNetwork.png "One Neural Network Results") <br />
Each Neural Network Structure has a table like above (The one above is a fcnn{7,5}). Each column is a neural network that trained on the map titled by the header of that column. For instance, "Asteroids.png" column shows the performance of a neural network that was trained on "Asteroids.png". However, the scores of these nns is "normalized" to the score of the neural network that trained on that particular map, which means each score is divided by the score of the nn trained on that map. Relative to the table, "normalization" happens by row. **Hypothetically/theoretically**, this is meant to show a "maximum attainable value for generalization". A value of .5 should be interpreted as that particular neural network being able to generalize half as well as it can train, or in a more common sense, can be thought of as a hypothetical accuracy or generalization score. Normalization returns values between 0 and 1, but there are instances where some models perform better than the model trained on that map. We logically theorize with more data, trained performance on a map will be higher than tested performance, but certain environments may encourage smarter techniques to be learned faster thus leading to those models performing better than the trained model. <br />
To reduce the significance of maps to learning, we train a model on multiple maps. The theory behind this is that if we trained across a very large amount of maps, testing the generalization will be more pertinent to the model instead of the map. Additionally, all models are trained and tested on the same maps, so if a particular map is easier to train on, it should be irrelevant of individual maps. <br />
<br />
![alt-text](src/main/resources/static/images/ExcelPreview.png "Excel Preview") <br />
The above table is where each neural network is compared against each other across the different maps. Each value is the average of all normalized scores of a nn trained on the column map. For instance, the data corresponding to the row: {7,5} and column: "Asteroids.png", shows the average of the normalized scores on all maps for the neural network with structure {7,5} and trained on Asteroids.png. In other words, it shows how well, on average, a neural network with structure {7,5} trained on "Asteroids.png" will perform on other maps compared to a neural network trained on that other map; in simpler terms, this is a measurement of generalization. Taking it a step further, we can compare this value to different neural networks or maps. If we compare it to another value in the same column, the training map stays the same, but the neural network structure changes, so the value comparison will tell you which neural network on average performs better on other maps; AKA, which neural network can generalize better. If we compare values on the same row, the neural network stays the same, but the training map changes, so the value comparison will show which maps are easier to generalize to. Going one more step further, the average of the rows represents the generalization of the neural networks irrelevant of what map it is trained on. The average of the column will show, which maps are easier to generalize onto.<br />

#### Conclusions
**AMOUNT OF DATA** is the biggest concern. Currently, if the nns are not trained enough on their training map, their scores may not be truly reflective of the hypothetical maximum achievable through generalization. This can cause outliers to be formed when normalizing the scores to measure the generalization. With small amounts of data, outliers have strong influence over outcome. Replication and consistent results will get around this by showing a pattern of recurring events.<br />
<br />
However, an example of a conclusion we could make would look like this: Using the second table that compares nn structures and maps, over many replications, neural networks that contain 9 input neurons routinely have a higher generalization average than neural networks with 7 input neurons, which shows a correlation of better generalization for neural networks that take in more information, or more specifically, the x and y position (which were the added two inputs), have a significant impact on the generalization of neural networks in this environment. <br />
<br />
By comparing the nns and the avg generalization over multiple repetitions, we can develop correlations between the structure and the generalization.

***

## Plans
- Come up with a way to load/display NNs
  - Maybe it should load populations and not just individuals
  - Display from the csv file?
- Write the implementation for NEAT
  - Adjust how FC and NEAT are connected
- Display on JPanel the stats with the image as well as maybe the best FNN, hyperparameters and A sort of display of the NNs connections
  - Add a key for the colors
- Make a generic class for Individuals to follow/inherent?
- Make Population Generic?
- Run multiple populations and use the best offspring to make a new population
  - This could also be a way to compete different structures against each other as well

### Problems
- Not enough smiles

### Goals and Ambitions
- Map Generalization Study
  - The maps resemble mazes or races, but the AI doesn't change. This can be used to study how AIs generalize when you change hyperparameters, FNN structure/type, or reward functions.
  - There also could be another ML model that changes the Learning model somehow based of off the respective map.
- Reinforcement Learning Model Comparisons
  - Genetic Algorithm training a Fully Connected Neural Network
  - Back Propogation trained Fully Connected Neural Network
  - Neural Evolution of Augmenting Topologies (NEAT)
    - Basically a genetic algorithm that trains a FNN, but the FNN isn't fully connected; the structure changes with the GA
  - Each of these models above can be run with different FNN structures and input and output spaces.

### How to reach the ambitions
RL Analysis:<br/>

| FNN Structure | GA + FNN | BackPropogation + FNN | NEAT |
|:-------------|---------|----------------------|------|
| 1            |         |                      |      |
| ...          |         |                      |      |
| n            |         |                      |      |

Each blank spot in the table is a trainable model where the hyperparameters can be tuned and the model can be shown in a reward vs generation graph

### How to run the code
In dev mode,

Navigate to the UI folder and run 'npm run mvn' and then run 'npm start'
After that is done, run the jar file 'java -jar target/app-0.0.1-SNAPSHOT.jar'

In prod mode,

Either run 'mvn clean install' or navigate to the UI folder and run 'npm run mvn'
After that is done, run the jar file 'java -jar target/app-0.0.1-SNAPSHOT.jar'