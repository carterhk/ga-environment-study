import React from 'react';
import { ReactPainter } from 'react-painter';
import { useState, useEffect } from 'react';
import $ from 'jquery';
import './App.css';
import RaceTrack2 from './RaceTrack2.png';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import UploadFileIcon from '@mui/icons-material/UploadFile';

function App() {

    const [selectedFile, setSelectedFile] = useState();
    const [image, setImage] = useState(RaceTrack2);
       
    // Code from https://www.geeksforgeeks.org/file-uploading-in-react-js/
    // On file select (from the pop up)
    const onFileChange = event => {
    
        // Update the state
        setSelectedFile(event.target.files[0])
    
    };
       
    // Half the code from Andrew half from https://www.geeksforgeeks.org/file-uploading-in-react-js/
    // On file upload (click the upload button)
    const onFileUpload = () => {
       
        // Create an object of formData
        const formData = new FormData();
       
        // Update the formData object
        formData.append(
          "file",
          selectedFile,
        //   selectedFile.name
        );
       
        // Details of the uploaded file
        console.log(selectedFile);
        console.log(formData.get('file'));

        // Andrew half
        // Uploads the file to the backend and also sets it as the image on the front end
        fetch('/api/uploadFile', {
            method: "POST", 
            body: formData
        })
        .then((response) => {
            if (response.ok) {
              setImage(URL.createObjectURL(selectedFile));
              alert('File upload success');
              return response;
            }
            alert('File upload fail');
            throw new Error('Something went wrong');
          })
          .then((responseJson) => {
            // Do something with the response
          })
          .catch((error) => {
            console.log(error)
          });

        setSelectedFile();
    };
       
    // Code from https://www.geeksforgeeks.org/file-uploading-in-react-js/
    // File content to be displayed after
    // file upload is complete
    const fileData = () => {
       
        if (selectedFile) {
            
          return (
            <div>
              <h2>File Details:</h2>
              <p>File Name: {selectedFile.name}</p>
    
              <p>File Type: {selectedFile.type}</p>
    
              <p>
                Last Modified:{" "}
                {selectedFile.lastModifiedDate.toDateString()}
              </p>
    
            </div>
          );
        } else {
          return (
            <div>
              <br />
              <h4>Choose before Pressing the Upload button</h4>
            </div>
          );
        }
    };

    const [population, setPopulation] = useState([]);

    var myInterval;

    // Andrew
    // Function run when stop is pushed
    const stopApp = () => {
        fetch('/stop')
        clearInterval(myInterval);
    }

    // Andrew
    // Function run when run is pushed
    const runForReal = () => {
        fetch('/run');
        myInterval = setInterval(fetchDataBuildScreen, 250);
    }

    const url = '/api/population';

    // Andrew
    // Gets the population data from the back end and builds the screen accordingly
    const fetchDataBuildScreen = async () => {
        try {
            const response = await fetch(url);
            const json = await response.json();
            setPopulation(json.population);
            var htmlElements = "";
            for (var i = 0; i < json.population.length; i++) {
                htmlElements += `<div id="individual${i}" class="individual"></div>`;
            }
            var container = document.getElementById("place-with-individuals");
            container.innerHTML = htmlElements;
            var pos = $('.place-with-individuals').offset();
            for (var j = 0; j < json.population.length; j++) {
                var top = json.population[j].y;
                var left = json.population[j].x;
                
                $(`#individual${j}`).css({
                position:'absolute',
                backgroundColor:`rgba(${json.population[j].color.red}, ${json.population[j].color.green}, ${json.population[j].color.blue}, ${json.population[j].color.alpha})`,
                top:top,
                left:left,
                transition: 'top 0.25s, left 0.25s'
                });
            }
        } catch (error) {
            console.log("error", error);
        }
    };

    // Refreshes the page when restart is clicked
    // From here https://upmostly.com/tutorials/how-to-refresh-a-page-or-component-in-react
    function refreshPage() {
        window.location.reload(false);
    }

    useEffect(() => {
        fetchDataBuildScreen();
    }, []);

    return (
        <Box
            sx={{
            '& > *': {
                m: 3,
            },
            }}
        >
            <Box
                sx={{
                    display: 'flex',
                    '& > *': {
                        m: 1,
                    },
                    }}
            >
                <ReactPainter
                    className="paint-zone"
                    width={500}
                    height={300}
                    onSave={blob => console.log(blob)}
                    render={({ triggerSave, canvas, imageDownloadUrl }) => (
                        <div>
                            <Box
                                sx={{
                                    display: 'flex',
                                    '& > *': {
                                        m: 1,
                                    },
                                    }}
                            >
                                <ButtonGroup variant="outlined" aria-label="outlined button group">
                                    <Button onClick={refreshPage}>Restart</Button>
                                    <Button onClick={triggerSave}>Save Canvas</Button>
                                    {imageDownloadUrl ? (
                                        <Button href={imageDownloadUrl} download>
                                            Download
                                        </Button>
                                    ) : null}
                                </ButtonGroup>
                            </Box>
                            <Box
                                sx={{
                                    border: '1px solid black',
                                  }}
                            >
                                {canvas}
                            </Box>
                        </div>
                    )}
                />
            </Box>
            <ButtonGroup size="large" variant="contained" aria-label="outlined primary button group">
                <Button onClick={runForReal} color='success'>Run Algorithm</Button>
                <Button onClick={stopApp} color='error'>Stop Algorithm</Button>
            </ButtonGroup>
            <div className='algoRunZone'>
                <img className="image" src={image} alt='algorithm runs on' />
                <div id='place-with-individuals' className='place-with-individuals'>
                </div>
            </div>
            <Box
                sx={{
                    display: 'flex',
                    '& > *': {
                        m: 1,
                    },
                    }}
            >
                <Button
                    component="label"
                    variant="outlined"
                    startIcon={<UploadFileIcon />}
                    >
                    Upload File
                    <input
                        type="file"
                        hidden
                        onChange={onFileChange}
                    />
                </Button>
                <Box>{selectedFile ? selectedFile.name : null}</Box>
                <Button variant="contained" onClick={onFileUpload}>
                  Upload!
                </Button>
            </Box>
            {fileData()}
        </ Box>
    );
}

export default App;