#  Created by Carter Koehn on 2023.3.20
#  Copyright © 2023 Carter Koehn. All rights reserved.

import matplotlib.pyplot as plt
import numpy as np
import csv

nns = {}

with open('FCNN_Testing.csv', newline='') as csvfile:
    file = csv.reader(csvfile, delimiter=',', quotechar='"')
    for line in file:
        if line[1] not in nns:
            nns[line[1]] = dict()
        else:
            nns[line[1]].update({line[0]: line[2]})


plt.rcParams['figure.figsize'] = [10, 6]
for agent in nns:
    randColor = np.random.rand(3,)
    x = []
    y = []
    for map in nns[agent]:
        x.append(map)
        y.append(float(nns[agent][map]))
    plt.plot(x, y, color=randColor, marker='o', lineStyle='solid', label=agent)

    # y_avg = [np.mean(y)] * len(x)
    # plt.plot(x, y_avg, color=randColor, marker='o', lineStyle='dashed')

plt.legend(loc='best')
plt.show()