/*
 * Created by Carter Koehn on 2023.4.20
 * Copyright © 2023 Carter Koehn. All rights reserved.
 */

package NEAT;

import com.gaenvironmentstudy.app.GeneticAlgorithm;
import com.gaenvironmentstudy.app.Individual;

import javax.sound.midi.SysexMessage;
import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class TestNEAT {

    public static void main(String[] args) {
        // System.out.println("Is it working like it should?: " + testAddNode());
        // paperExample();
        // fuggedProblem();
        // crossoverNullToNodes();
        // duplicateConnectionsNullToNode();
        testLoad();

        System.out.println("Everything ran!");
    }

    public static boolean testAddNode(){
        NEAT nn = new NEAT(new int[] {7,5});
        boolean ret = true;

        ret = ret & nn.addConnection(0,7, 10);
        ret = ret & nn.addConnection(1, 7, 10);
        nn.addNodeToConn(0);
        nn.addNodeToConn(1);

        // This one allows for feedforwarding to try to use an unactivated node
        ret = ret & nn.addConnection(13,12,10);
        nn.addNodeToConn(3);

        // This one tries to make a cycle between 13 -> 12, 12 -> 14, 14 -> 13
        ret = ret & !nn.addConnection(14,13,10);


        //------------------------------------------------------------------------------------------------------
        // Testing FeedForwarding

        // This tests that it CAN run feedforward twice in a row, should be different
        System.out.println(nn.feedForward(new double[] {1,1,1,1,0,0,0}));
        System.out.println(nn.feedForward(new double[] {10,1,1,1,0,0,0}));

        // This tests that it resets the values correctly too, should be the same
        System.out.println(nn.feedForward(new double[] {1,1,1,1,0,0,0}));
        System.out.println(nn.feedForward(new double[] {1,1,1,1,0,0,0}));

        return ret;
    }

    public static void paperExample(){
        NEAT nn = new NEAT(new int[] {3,1});
        nn.addConnection(0,3, 1);
        nn.addConnection(1,3, 1);
        nn.addConnection(2,3, 1);
        nn.addNodeToConn(1);
        nn.addConnection(0,4, 1);
        nn.addNodeToConn(2);
        nn.addConnection(1,3, 1);
        System.out.println(nn.feedForward(new double[] {1,1,1}));
        System.out.println(nn);
    }

    public static void fuggedProblem(){
        NEAT nn = new NEAT(new int[] {9,5});
        nn.addConnection(7,10,1);
        System.out.println(nn.getConnections().values());

        System.out.println(nn.getNodes().get(7));
        System.out.println(nn.getNodes().get(10));
        System.out.println(nn.getNodes().get(7).equals(nn.getNodes().get(10)));
        System.out.println(nn.getConnections().values().contains(new Connection(nn.getNodes().get(7), nn.getNodes().get(11), 1.0, true, 1)));

    }

    public static void crossoverNullToNodes(){
        NEAT nn1;
        NEAT nn2;
        GeneticAlgorithm ga;
        ga = new GeneticAlgorithm();
        // This only happens sometimes because there's a chance it won't inherit it
        for (int i = 0; i < 100; i++){
            nn1 = new NEAT(new int[] {9,5});
            nn2 = new NEAT(new int[] {9,5});

            nn1.addConnection(0, 10, 1);
            nn2.addConnection(0, 10, 2);
            nn1.addNodeToConn(0);
            ga.NEATCrossover(new Individual(nn1, Color.GREEN), new Individual(nn2, Color.GREEN));
        }
    }

    public static void duplicateConnectionsNullToNode(){
        Random rand = new Random();
        NEAT nn1;
        NEAT nn2;
        GeneticAlgorithm ga;
        ga = new GeneticAlgorithm();
        ArrayList<Node> options = new ArrayList<Node>();

        // This only happens sometimes because there's a chance it won't inherit it
        for (int i = 0; i < 1; i++){
            nn1 = new NEAT(new int[] {9,5});
            nn2 = new NEAT(new int[] {9,5});

            nn1.addConnection(0, 10, 1);
            nn2.addConnection(0, 10, 2);
            nn1.addNodeToConn(0);
            nn1.addNodeToConn(0);


            ga.NEATCrossover(new Individual(nn1, Color.GREEN), new Individual(nn2, Color.GREEN));
        }
    }

    public static void testLoad(){
        NEAT nn1 = new NEAT("src/main/resources/static/data/NEAT.csv");

        System.out.println(nn1);

        nn1.addConnection(0, 10, 1);
        nn1.addNodeToConn(0);

        nn1.feedForward(new double[] {0, 0, 0, 0, 0, 0, 0, 0, 0,});
    }
}
