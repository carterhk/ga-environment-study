/*
 * Created by Carter Koehn on 2023.4.20
 * Copyright © 2023 Carter Koehn. All rights reserved.
 */

package NEAT;

import com.gaenvironmentstudy.app.Inputs;

import java.util.ArrayList;
import java.util.Random;

public class Node implements Comparable{
    private Random rand = Inputs.rand;

    public enum TYPE {
        INPUT,
        HIDDEN,
        OUTPUT,
        ;
    }


    private ArrayList<Connection> inConnections;
    private ArrayList<Connection> outConnections;
    private TYPE type;
    private int id;
    private double value;
    private double bias;
    private boolean activated;

    public Node(TYPE type, int id) {
        this.type = type;
        this.id = id;

        inConnections = new ArrayList<Connection>();
        outConnections = new ArrayList<Connection>();
        activated = false;
        if (type.equals(TYPE.HIDDEN)){
            value = rand.nextDouble();
            bias = rand.nextDouble();
        }
    }

    public Node(Node gene) {
        this.type = gene.type;
        this.id = gene.id;

        inConnections = new ArrayList<Connection>();
        outConnections = new ArrayList<Connection>();
        activated = false;
        if (type.equals(TYPE.HIDDEN)){
            value = rand.nextDouble();
            bias = rand.nextDouble();
        }
    }

    public TYPE getType() {
        return type;
    }
    public int getId() {
        return id;
    }
    public ArrayList<Connection> getInConnections() {
        return inConnections;
    }
    public ArrayList<Connection> getOutConnections() {
        return outConnections;
    }
    public boolean isActivated() {
        return activated;
    }
    public double getBias() {
        return bias;
    }
    public void setBias(double bias) {this.bias = bias;}
    public void addToBias(double bias) {this.bias += bias;}
    public double getValue() {
        return value;
    }
    public void setValue(double value) {
        this.value = value;
    }
    public void addToValue(double value) {
        this.value += value;
    }
    public void addIncoming(Connection conn){
        inConnections.add(conn);
    }
    public void addOutgoing(Connection conn){
        outConnections.add(conn);
    }

    public ArrayList<Node> getAllDescendants(){
        return descendantHelper(new ArrayList<Node>());
    }

    public ArrayList<Node> descendantHelper(ArrayList<Node> descendants){
        for (Connection conn : inConnections){
            if (!conn.isExpressed()) continue;
            if (descendants.contains(conn.getFromNode())) continue;
            descendants.add(conn.getFromNode());
            descendants = conn.getFromNode().descendantHelper(descendants);
        }
        return descendants;
    }

    public boolean eligible(){
        boolean ret = true;
        for (Connection conn : inConnections){
            ret = conn.getFromNode().activated || conn.getFromNode().type == TYPE.INPUT;
        }
        return ret;
    }

    /**
     * Individual nodes are fed forward by previous connections
     * This is friggin genius. As long as there are no cycles in the NN
     * this way allows it to work regardless of order
     */
    public void feedForward(){
        for (Connection conn : inConnections){
            value += conn.getWeight() * conn.getFromNode().getValue();
        }
        activate();
    }

    /**
     * Activating applies the sigmoid function to the value + bias
     */
    public void activate(){
        value += bias;
        // value = 1 / (1 + Math.exp(-value));
        value = Math.max(value, 0);
        activated = true;
    }

    public void reset(){
        activated = false;
        value = 0;
    }

    @Override
    public int compareTo(Object o) {
        Node other = (Node) o;
        return (int) (value - other.getValue());
    }

    @Override
    public String toString(){
        return String.format("(id: %d, value:%2.2f)", id, value);
    }

    @Override
    public boolean equals(Object o){
        if (o == null) return false;
        if (o.getClass() != this.getClass()) return false;
        Node other = (Node) o;
        return id == other.getId();
    }

}
