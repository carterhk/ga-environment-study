/*
 * Created by Carter Koehn on 2023.4.22
 * Copyright © 2023 Carter Koehn. All rights reserved.
 */

package NEAT;

import com.gaenvironmentstudy.app.Individual;

import java.util.ArrayList;
import java.util.Collections;

public class Species {
    private Individual representative;
    private ArrayList<Individual> members;

    public Species(Individual representative){
        this.representative = representative;
        members = new ArrayList<Individual>();
        members.add(representative);
    }

    public int numMembers(){
        return members.size();
    }
    public Individual get(int i){
        return members.get(i);
    }

    public boolean addMember(Individual member){
        if (((NEAT)representative.getNN()).compatibleWith((NEAT)member.getNN())){
            members.add(member);
            return true;
        }
        return false;
    }

    public void adjustFitness(){
        for (Individual member : members){
            member.setFitness(member.getFitness() / members.size());
        }
    }

    public double getTotalFitness(){
        int sum = 0;
        for (Individual member : members){
            sum += member.getFitness();
        }
        return sum;
    }

    public ArrayList<Individual> getBest(int amount) {
        ArrayList<Individual> selected = new ArrayList<Individual>();
        Collections.sort(members);
        Collections.reverse(members);
        for (int i = 0; i < amount; i++){
            selected.add(members.get(i));
        }
        return selected;
    }
}
