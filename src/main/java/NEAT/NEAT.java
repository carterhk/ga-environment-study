/*
 * Created by Carter Koehn on 2023.4.20
 * Copyright © 2023 Carter Koehn. All rights reserved.
 */

package NEAT;

import com.gaenvironmentstudy.app.Inputs;
import com.gaenvironmentstudy.app.NeuralNetwork;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class NEAT extends NeuralNetwork {
    private HashMap<Integer, Node> nodes;
    private HashMap<Integer, Connection> connections;
    private int connectionInnovation, nodeInnovation;
    private int inputSize, outputSize;

    public NEAT(int[] structure) {
        this.inputSize = structure[0];
        this.outputSize = structure[1];
        nodes = new HashMap<>();
        connections = new HashMap<>();
        connectionInnovation = 0;
        nodeInnovation = 0;

        for (int i = 0; i < inputSize; i++){
            int id = nodeInnovation++;
            nodes.put(id, new Node(Node.TYPE.INPUT, id));
        }
        for (int i = 0; i < outputSize; i++){
            int id = nodeInnovation++;
            nodes.put(id, new Node(Node.TYPE.OUTPUT, id));
        }
    }

    public NEAT(NEAT clone){
        this.inputSize = clone.getInputSize();
        this.outputSize = clone.getOutputSize();
        this.nodes = clone.getNodes();
        this.connections = clone.getConnections();
        this.connectionInnovation = connections.size();
        this.nodeInnovation = nodes.size();
    }

    public NEAT(String filePath){
        nodes = new HashMap<>();
        connections = new HashMap<>();
        connectionInnovation = 0;
        nodeInnovation = 0;

        try {
            CSVReader csvReader = new CSVReader(new FileReader(filePath));
            String[] line = csvReader.readNext();

            this.inputSize = Integer.parseInt(line[0]);
            this.outputSize = Integer.parseInt(line[1]);

            for (int i = 0; i < inputSize; i++){
                int id = nodeInnovation++;
                nodes.put(id, new Node(Node.TYPE.INPUT, id));
            }
            for (int i = 0; i < outputSize; i++){
                int id = nodeInnovation++;
                nodes.put(id, new Node(Node.TYPE.OUTPUT, id));
            }

            while (!(line = csvReader.readNext())[0].equals("Nodes")){
                if (line[0].equals("#")){
                    continue;
                }

                addConnection(Integer.parseInt(line[0]),
                        Integer.parseInt(line[1]),
                        Double.parseDouble(line[2]));

                if (!Boolean.parseBoolean(line[3])){
                    addNodeToConn(connectionInnovation - 1);
                }
            }

            while ((line = csvReader.readNext()) != null){
                if (line[0].equals("#")){
                    continue;
                }
                nodes.get(Integer.parseInt(line[0])).setBias(Double.parseDouble(line[1]));
            }

            csvReader.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public HashMap<Integer, Node> getNodes() {
        return nodes;
    }
    public HashMap<Integer, Connection> getConnections() {
        return connections;
    }
    public int getInputSize() {
        return inputSize;
    }
    public int getOutputSize() {
        return outputSize;
    }

    /**
     * Adds a node on the param connection
     * The existing connection is disabled
     * The incoming weight is the weight of the old connection, the new connection weight is zero
     * @param connectionID the id of the connection that will have a node added
     */
    public boolean addNodeToConn(int connectionID) {
        if (!connections.get(connectionID).isExpressed()) return false;
        int id = nodeInnovation++;
        Node newNode = new Node(Node.TYPE.HIDDEN, id);
        nodes.put(id, newNode);

        Connection conn = connections.get(connectionID);

        addConnection(conn.getFromNode().getId(), newNode.getId(), conn.getWeight());
        addConnection(newNode.getId(), conn.getToNode().getId(), conn.getWeight());

        conn.disable();
        return true;
    }

    public ArrayList<Node> getLegalConnections(Node fromNode){
        if (fromNode.getType().equals(Node.TYPE.OUTPUT)) return new ArrayList<Node>();

        ArrayList<Node> options = new ArrayList<Node>();
        options.addAll(nodes.values());
        for (Node node : nodes.values()){
            if (node.getType().equals(Node.TYPE.INPUT)) options.remove(node);
            else break;
        }
        // Get rid of existing connections including disabled ones
        for (Connection conn : connections.values()){
            if (!conn.getFromNode().equals(fromNode)) continue;
            options.remove(conn.getToNode());
        }
        options.removeAll(fromNode.getAllDescendants());
        options.remove(fromNode);
        return options;
    }

    public boolean addConnection(int fromId, int toId, double weight) {
        Node fromNode = nodes.get(fromId);
        Node toNode = nodes.get(toId);

        if (toId == fromId) return false;
        if (fromNode.getType() == Node.TYPE.OUTPUT) return false;
        if (toNode.getType() == Node.TYPE.INPUT) return false;
        if (connections.values().contains(new Connection(fromNode, toNode, weight, true, 1))) return false;
        if (fromNode.getAllDescendants().contains(toNode)) return false;

        int id = connectionInnovation++;
        Connection newConn = new Connection(fromNode, toNode, weight, true, id);

        fromNode.addOutgoing(newConn);
        toNode.addIncoming(newConn);
        connections.put(id, newConn);
        return true;
    }

    public boolean addConnection(Connection conn) {
        // System.out.println("Adding Connection: " + conn.toString());
        if (!conn.isExpressed()){
            addConnection(conn.getFromNode().getId(), conn.getToNode().getId(), conn.getWeight());
            addNodeToConn(connectionInnovation - 1);
            return true;
        }
        return addConnection(conn.getFromNode().getId(), conn.getToNode().getId(), conn.getWeight());
    }

    public Connection getConnection(Connection other){
        for (Connection conn : connections.values()){
            if (conn.equals(other)){
                return conn;
            }
        }
        System.out.println("Connection not found!");
        return null;
    }

    /**
     * Iterates through every node by creating a working list that adds nodes
     * Doesn't add duplicates
     * Skips activated nodes
     * if a previous node is not activated, it moves it to the back of the list
     * This implementation chooses a node, then gets all of its precessing nodes
     *
     * Instead of using an arraylist for toBeActivated, it would be better as a heap
     *
     * @param inputs
     * @return
     */
    @Override
    public ArrayList<Node> feedForward(double[] inputs) {
        // System.out.println("\n\nFeeding through: " + this.toString());

        ArrayList<Node> toBeActivated = new ArrayList<Node>();
        int inputCounter = 0;
        for (Node node : nodes.values()){
            node.reset();
            if (node.getType().equals(Node.TYPE.INPUT)){
                toBeActivated.add(node);
                node.setValue(inputs[inputCounter++]);
            }
        }

        for (int i = 0; i < toBeActivated.size(); i++){
            if (toBeActivated.get(i).isActivated()) continue;
            for (Connection conn : toBeActivated.get(i).getOutConnections()){
                if (!conn.isExpressed()) continue;
                if (toBeActivated.contains(conn.getToNode())) continue;
                toBeActivated.add(conn.getToNode());
            }

            if (toBeActivated.get(i).getType() == Node.TYPE.INPUT) continue;
            if (!toBeActivated.get(i).eligible()) {
                toBeActivated.add(toBeActivated.size(), toBeActivated.get(i));
                continue;
            }

            // node.feedForward calls node.activate();
            // System.out.println(String.format("Propagating: %d", toBeActivated.get(i).getId()));
            toBeActivated.get(i).feedForward();
        }

        ArrayList<Node> ret = new ArrayList<Node>();
        for (Node node : nodes.values()){
            if (node.getType().equals(Node.TYPE.OUTPUT)){
                ret.add(node);
            }
        }
        return ret;
    }

    @Override
    public int getMaxOutput() {
        ArrayList<Node> out = new ArrayList<Node>();
        for (Node node : nodes.values()){
            if (node.getType() == Node.TYPE.OUTPUT){
                out.add(node);
            }
        }
        return out.indexOf(Collections.max(out));
    }

    public boolean compatibleWith(NEAT other){
        ArrayList<Connection> disjoint = new ArrayList<Connection>();
        double sumWeightDifferences = 0;
        int numConnections = 0;
        for (Connection conn : connections.values()){
            if (!other.getConnections().values().contains(conn)){
                disjoint.add(conn);
            }
            else{
                sumWeightDifferences += Math.abs(conn.getWeight() - other.getConnection(conn).getWeight());
                numConnections++;
            }
        }
        for (Connection conn : other.getConnections().values()){
            if (!connections.values().contains(conn) && !disjoint.contains(conn)){
                disjoint.add(conn);
            }
        }


        if (numConnections == 0) return disjoint.size() < Inputs.COMPATIBLITY_THRESHOLD;
        // System.out.println("Average weight difference: " + (sumWeightDifferences / numConnections));
        return (sumWeightDifferences / numConnections) + disjoint.size() < Inputs.COMPATIBLITY_THRESHOLD;

    }

    @Override
    public void save() {
        File file = new File("src/main/resources/static/data/NEAT.csv");
        try {
            CSVWriter writer = new CSVWriter(new FileWriter(file));
            writer.writeNext(new String[] {Integer.toString(inputSize), Integer.toString(outputSize)});
            for (Connection conn : connections.values()){
                writer.writeNext(new String[] {Integer.toString(conn.getFromNode().getId()),
                        Integer.toString(conn.getToNode().getId()),
                        Double.toString(conn.getWeight()),
                        Boolean.toString(conn.isExpressed())});
            }
            writer.writeNext(new String[] {"Nodes"});

            for (Node node : nodes.values()){
                writer.writeNext(new String[] {
                        Integer.toString(node.getId()),
                        Double.toString(node.getBias())
                });
            }

            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getType(){
        return "NEAT";
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (!o.getClass().equals(this.getClass())) return false;

        NEAT other = (NEAT) o;
        return connections.equals(other.connections);
    }

    @Override
    public String toString(){
        StringBuilder s = new StringBuilder();
        int counter = 0;
        for (Connection conn : connections.values()){
            if (counter++ % 7 == 0){
                s.append("\n");
            }
            s.append(conn.toString());
            s.append(", ");

        }
        return s.toString();
    }
}
