/*
 * Created by Carter Koehn on 2023.4.20
 * Copyright © 2023 Carter Koehn. All rights reserved.
 */

package NEAT;

public class Connection {

    private Node fromNode;
    private Node toNode;
    private double weight;
    private boolean expressed;
    private int id;

    public Connection(Node fromNode, Node toNode, double weight, boolean expressed, int id) {
        this.fromNode = fromNode;
        this.toNode = toNode;
        this.weight = weight;
        this.expressed = expressed;
        this.id = id;
    }

    public Connection(Connection con) {
        this.fromNode = con.fromNode;
        this.toNode = con.toNode;
        this.weight = con.weight;
        this.expressed = con.expressed;
        this.id = con.id;
    }

    public Node getFromNode() {
        return fromNode;
    }
    public Node getToNode() {
        return toNode;
    }
    public double getWeight() {
        return weight;
    }
    public void setWeight(double newWeight) {
        this.weight = newWeight;
    }
    public void addToWeight(double newWeight) {
        this.weight += newWeight;
    }
    public boolean isExpressed() {
        return expressed;
    }
    public void disable() {
        expressed = false;
    }
    public int getId() {
        return id;
    }


    public Connection copy() {
        return new Connection(fromNode, toNode, weight, expressed, id);
    }

    @Override
    public String toString(){
        return String.format("(%d: %d -> %d%s", id, fromNode.getId(), toNode.getId(), expressed ? ")" : ", dis)");
    }

    @Override
    public boolean equals(Object o){
        if (o == null) return false;
        if (!o.getClass().equals(this.getClass())) return false;
        Connection other = (Connection) o;
        boolean ret = true;
        ret &= toNode.equals(other.getToNode());
        ret &= fromNode.equals(other.getFromNode());
        ret &= (expressed == other.isExpressed());
        return ret;
    }
}
