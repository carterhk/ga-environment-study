/*
 * Created by Carter Koehn on 2023.2.14
 * Copyright © 2023 Carter Koehn. All rights reserved.
 */
package com.gaenvironmentstudy.app;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.awt.*;

import FNN.FullyConnectedNN;

/**
 * This is the JPanel display
 */
public class Screen extends JPanel{
    private final int STATS_WIDTH = 200;
    private final int STATS_HEIGHT = 350;

    private BufferedImage background;
    private String bgImagePath;
    private ArrayList<Individual> individuals;
    private Individual bestIndividual;
    private int height;
    private int width;
    private int numAlive;

    /**
     * Default Constructor that starts the screen with a background image
     * @param bgImagePath string path to background image
     */
    public Screen(String bgImagePath){
        this.bgImagePath = bgImagePath;
        try {
            this.background = ImageIO.read(new File(bgImagePath));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        individuals = new ArrayList<Individual>();
        height = background.getHeight() + STATS_HEIGHT;
        width = background.getWidth() + STATS_WIDTH;
    }

    public int getHeight(){
        return height - STATS_HEIGHT;
    }
    public int getWidth(){
        return width - STATS_WIDTH;
    }
    public void setBest(Individual bestIndividual){
        this.bestIndividual = bestIndividual;
    }
    public void setIndividuals(ArrayList<Individual> newIndividuals) {
        individuals = newIndividuals;
    }
    public void setNumAlive(int numAlive) {
        this.numAlive = numAlive;
    }
    public void setBackground(String bgImagePath){
        try {
            this.bgImagePath = bgImagePath;
            this.background = ImageIO.read(new File(bgImagePath));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        height = background.getHeight() + STATS_HEIGHT;
        width = background.getWidth() + STATS_WIDTH;
    }
    public String getBackgroundPath(){
        return bgImagePath;
    };

    /**
     * isColliding uses the param coordinates and the background. If the pixel at the
     * coordinates is a barrier (Black pixel or out of bounds) it will return true.
     *
     * @return True if Colliding, false else
     */
    public boolean isColliding(int x, int y){
        for (int i = 0; i < Individual.SIZE; i++) {
            for (int j = 0; j < Individual.SIZE; j++) {
                if (x + i < 0 || y + j < 0 || x + i + Individual.SIZE > width - STATS_WIDTH || y + j + Individual.SIZE > height - STATS_HEIGHT){
                    return true;
                }
                if (Color.BLACK.equals(new Color(background.getRGB(x + i, y + j)))){
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isCollidingCircle(double x, double y, double radius) {
        for (double i = x - radius; i <= x + radius; i++) {
            for (double j = y - radius; j <= y + radius; j++) {
                if (i < 0 || j < 0 || i >= width - STATS_WIDTH || j >= height - STATS_HEIGHT){
                    return true;
                }
                double dist = Math.sqrt(Math.pow(i - x, 2) + Math.pow(j - y, 2));
                if (dist <= radius && Color.BLACK.equals(new Color(background.getRGB((int)i, (int)j)))){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * This is the draw function of JPanel. Anything drawn here will be updated on
     * the panel
     */
    @Override
    public void paintComponent(Graphics g){
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(new Color(210,180,140));
        g2d.fillRect(0,0, width, height);
        g2d.drawImage(background, null, 0, 0);
        g2d.setColor(Color.black);
        g2d.drawRect(0,0, width - STATS_WIDTH, height - STATS_HEIGHT);


        drawIndividuals(g);
        drawNumAliveBox(g);
        // if (Inputs.DEFAULT_NEURALNETWORK.getType().equals("FNN")) drawNeuralNetwork(g);
    }

    /**
     * Responsible for drawing the Individuals onto the screen
     */
    private void drawIndividuals(Graphics g){
        try {
            for (Individual i : individuals) {
                g.setColor(i.getColor());
                g.fillRect(i.getx(), i.gety(), Individual.SIZE, Individual.SIZE);

                //Draw little header line
                if (Inputs.SHOW_AI_VISION) {
                    g.setColor(Color.BLUE);
                    g.drawLine(i.getx() + Individual.SIZE / 2, i.gety() + Individual.SIZE / 2, (int) (i.getx() + 8 * Math.cos(Math.toRadians(i.getTheta())) + Individual.SIZE / 2), (int) (i.gety() + 8 * Math.sin(Math.toRadians(i.getTheta()))) + Individual.SIZE / 2);
                }
            }
        } catch (Exception ignore){}
    }

    /**
     * Draw a little box that shows how many are left alive
     * @param g graphics manipulator
     */
    private void drawNumAliveBox(Graphics g){
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.black);
        g2d.setFont(new Font("Courier New", Font.PLAIN, 14));
        g2d.drawString("Num Alive: " + numAlive, Math.max(width - 150, 0), 25);
    }

    /**
     * Draws the neural network of the best individual (Highlighted in black)
     * @param g graphics manipulator
     */
    private void drawNeuralNetwork(Graphics g) {
        if (bestIndividual == null){
            return;
        }

        Graphics2D g2d = (Graphics2D) g;
        bestIndividual.setColor(Color.BLACK);
        FullyConnectedNN NN = (FullyConnectedNN) bestIndividual.getNN();

        for (int w = 0; w < NN.getNumLayers(); w++){
            int nodeGapRatio = 1;
            int nodeDia = STATS_HEIGHT / (NN.getSizeOfLayer(w) + nodeGapRatio);
            int gap = nodeDia / (NN.getSizeOfLayer(w) + nodeGapRatio);

            for (int i = 1; i < NN.getSizeOfLayer(w) + 1; i++){
                int x = 200 + 50 * w;
                int y = height - (gap + nodeDia * i + gap * i);

                g2d.setColor(Color.BLACK);
                g2d.drawOval(x, y, nodeDia / 2, nodeDia / 2);
                g2d.setFont(new Font("Courier New", Font.PLAIN, 10));
                g2d.drawString(String.format("%3.0f", NN.getNodeAt(w, i-1).getValue()), x, y + nodeDia/4 + gap/4);
            }

        }

    }
}
