/*
 * Created by Carter Koehn on 2023.2.14
 * Copyright © 2023 Carter Koehn. All rights reserved.
 */

package com.gaenvironmentstudy.app;

import FNN.FullyConnectedNN;
import NEAT.NEAT;
import NEAT.Species;

import java.util.ArrayList;
import java.util.Collections;

/**
 * A population is a Data structure that holds individuals
 * Currently uses an arraylist to maintain individuals, could be worth using something else
 */
public class Population {
    private int size;
    private ArrayList<Individual> population;
    private ArrayList<Species> species;

    /**
     * Constructor
     * Adds the param # of individuals
     */
    public Population(int size){
        this.size = size;
        species = new ArrayList<Species>();
        population = new ArrayList<Individual>();
        for (int i = 0; i < size-1; i++) {
            if (Inputs.NN_TYPE.equals("FNN")) population.add(new Individual(new FullyConnectedNN(Inputs.NN_STRUCTURE)));
            else if (Inputs.NN_TYPE.equals("NEAT")) population.add(new Individual(new NEAT(Inputs.NN_STRUCTURE)));
        }
    }


    public ArrayList<Individual> getPopulation() {
        return population;
    }
    public ArrayList<Species> getSpecies() {
        return species;
    }
    public void remove(Individual toBeRemoved){
        population.remove(toBeRemoved);
    }
    public void removeAll(ArrayList<Individual> toBeRemoved){
        for (Individual individual : toBeRemoved){
            population.remove(individual);
        }
    }
    public void add(Individual toBeAdded){
        population.add(toBeAdded);
    }
    public void addAll(ArrayList<Individual> toBeAdded){
        for (Individual individual : toBeAdded){
            population.add(0, individual);
        }
    }
    public boolean allDead(){
        return getNumAlive() == 0;
    }
    public int getNumAlive(){
        int count = 0;
        for (Individual i : population){
            if (i.isAlive()){
                count++;
            }
        }
        return count;
    }

    /**
     * getFittest will find the fittest individuals of the population
     * Specifically, it will find top "amount" of Individuals and return them
     * in an ArrayList.
     * If amount = 0, it returns an empty list
     * If amount = 2, it returns a list with 2 fittest individuals in it
     *
     * The returned list will be in the respective order
     */
    public ArrayList<Individual> getFittest(int amount){
        ArrayList<Individual> selected = new ArrayList<Individual>();
        Collections.sort(population);
        Collections.reverse(population);
        for (int i = 0; i < amount; i++){
            selected.add(population.get(i));
        }
        return selected;
    }

    /**
     * getWorst will find the worst fit individuals of the population
     * Specifically, it will find bottom "amount" of Individuals and return them
     * in an ArrayList.
     * If amount = 0, it returns an empty list
     * If amount = 2, it returns a list with 2 worst individuals in it
     *
     * The returned list will be in the respective order
     */
    public ArrayList<Individual> getWorst(int amount){
        ArrayList<Individual> selected = new ArrayList<Individual>();
        Collections.sort(population);
        for (int i = 0; i < amount; i++){
            selected.add(population.get(i));
        }
        return selected;
    }

    /**
     * Calculates the total fitness for the population
     * It sums all of the Individuals fitness values
     * @return total Fitness
     */
    public double getTotalFitness() {
        int sum = 0;
        for (Individual i : population){
            sum += i.getFitness();
        }
        return sum;
    }

    public void speciate(){
        boolean foundSpecies;
        for (Individual ind : population){
            foundSpecies = false;

            for (Species specie : species){
                if (specie.addMember(ind)){
                    foundSpecies = true;
                    break;
                }
            }

            if (!foundSpecies){
                species.add(new Species(ind));
            }
        }

        for (Species specie : species){
            specie.adjustFitness();
        }
    }

    /**
     * Resets every individual in the population
     */
    public void reset(){
        species.clear();
        for (Individual i : population){
            i.reset();
        }
    }

    /**
     * String representation includes displaying every single individual within the population
     * @return a string representation
     */
    @Override
    public String toString(){
        StringBuilder s = new StringBuilder();

        s.append(String.format("Population with %d Individuals\n", population.size()));
        for (Individual ind : population) {
            s.append(ind + ", ");
        }
        s.append("\n");
        return s.toString();
    }
}
