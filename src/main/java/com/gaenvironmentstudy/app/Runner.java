/*
 * Created by Carter Koehn on 2023.2.14
 * Copyright © 2023 Carter Koehn. All rights reserved.
 */
package com.gaenvironmentstudy.app;
import javax.swing.*;
import java.awt.*;
import java.util.Random;

import static com.gaenvironmentstudy.app.AppApplication.screen;

/**
 * This is where the procedural backend happens.
 */
public class Runner extends JFrame{
    public static GeneticAlgorithm GA;
    static JFrame frame = new JFrame("Displaying com.gaenvironmentstudy.app.Screen");
    static Thread thread = new Thread();

    static public Random rand = Inputs.rand;
    static private String[] maps = {"src/main/resources/static/images/Maps/Default_White.png",
            "src/main/resources/static/images/Maps/Single_Barrier.png",
            "src/main/resources/static/images/Maps/Maze2.png",
            "src/main/resources/static/images/Maps/Maze1.png",
            "src/main/resources/static/images/Maps/RaceTrack1.png",
            "src/main/resources/static/images/Maps/Asteroids.png",
            "src/main/resources/static/images/Maps/RaceTrack2.png",
            "src/main/resources/static/images/Maps/Vs.png"};

    boolean stopped = false;

    public void run() {
        int i = 0;
        while (!stopped && i < Inputs.NUM_GENERATIONS) {
            runGeneration(GA);
            // This preserves the results of the last generation on the JPanel
            if (i < Inputs.NUM_GENERATIONS - 1){
                GA.train();
            }
            i++;
        }
    }

    /**
     * Initialises the JFrame
     */
    public void setup(){
        GA = new GeneticAlgorithm();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(screen.getWidth() + 10, screen.getHeight() + 10));
        frame.setLocationRelativeTo(null);
        frame.getContentPane().add(screen);
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * This is where the given population is simulated on the
     * big screen. At the end of it, it prints out some results from the
     * generation
     * @param GA
     */
    private static void runGeneration(GeneticAlgorithm GA){
        GA.genCount++;

        System.out.println("\n");
        System.out.println("Running simulation of generation: " + GA.genCount);
        System.out.println("popSize: " + GA.popSize +
                ", numParents: " + GA.numParents +
                ", numElites: " + GA.numElitists +
                ", mutScale: " + GA.mutateScale +
                ", mutRate: " + GA.probMutate);
        long startTime;
        long targetTime = 1000 / Inputs.FPS;
        long URDTimeMilli = 0;
        long waitTime = 0;
        screen.setIndividuals(GA.getPopulation().getPopulation());
        frame.setTitle("Generation: " + GA.genCount);

        // Random Backgrounds
        // screen.setBackground(maps[rand.nextInt(maps.length)]);

        while (!GA.getPopulation().allDead()){
            startTime = System.nanoTime();
            for (Individual i : GA.getPopulation().getPopulation()) {
                if (i.isAlive()) {
                    i.calcDistanceToWalls(screen);
                    i.feedForward(screen);
                    i.move(screen);
                }
            }

            screen.setNumAlive(GA.getPopulation().getNumAlive());       // Soon to be set stats

            screen.repaint();
            URDTimeMilli = (System.nanoTime() - startTime) / 1000000;
            waitTime = targetTime - URDTimeMilli;
            waitTime = Math.max(waitTime, 0);
            try {
                thread.sleep(waitTime);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        System.out.println("Average Fitness was: " + GA.getPopulation().getTotalFitness() / Inputs.DEFAULT_POPULATION_SIZE);
        System.out.println(String.format("Best Fitness was: %5.2f", GA.getBest().getFitness()));


        //! WARNING
        // Sometimes this won't return one that gets carried over because the elitists,
        // although not the same, may be the exact same score, but not carried over
        screen.setBest(GA.getPopulation().getFittest(1).get(0));

    }
}
