/*
 * Created by Carter Koehn on 2023.2.14
 * Copyright © 2023 Carter Koehn. All rights reserved.
 */
package com.gaenvironmentstudy.app;
import FNN.FullyConnectedNN;
import NEAT.*;
import com.opencsv.CSVWriter;

import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;

/**
 * Genetic Algorithms are models that resemble evolution in order to grow. They use survival of the fittest to reproduce and make
 * future generations until a solution is approximated
 */
public class GeneticAlgorithm {
    // Hyperparameters :)
    public int popSize;
    public int numParents;
    public int numElitists;
    public double mutateScale;
    public double probMutate;

    public int genCount = 0;
    private Population population;
    private Random rand = Inputs.rand;

    /**
     * CONSTRUCTOR with hyperparameters
     */
    public GeneticAlgorithm(int popSize, int numParents, int numElitists, double mutateScale, double probMutate){
        this.popSize = popSize;
        this.numParents = numParents;
        this.numElitists = numElitists;
        this.mutateScale = mutateScale;
        this.probMutate = probMutate;

        population = new Population(popSize);
    }

    /**
     * Default Constructor has default hyperparameters
     */
    public GeneticAlgorithm(){
        this(Inputs.DEFAULT_POPULATION_SIZE,
                Inputs.DEFAULT_NUM_PARENTS,
                Inputs.DEFAULT_NUM_ELITISTS,
                Inputs.DEFAULT_MUTATION_SCALE,
                Inputs.DEFAULT_PROB_MUTATE);
    }

    public Population getPopulation() {
        return population;
    }
    public void setPopulation(Population population) {
        this.population = population;
    }
    public Individual getBest(){
        return population.getFittest(1).get(0);
    }

    /**
     * Steps:
     * 1. Selects numParents amount
     * 2. Removes all that aren't elite, resets the elite
     * 3. Random choose 2 from the selected parents
     * 4. Crossover those 2
     * 5. "Mutate" the children
     *
     * @result the population becomes the next population ready to be simulated
     */
    public void train(){
        if (Inputs.NN_TYPE.equals("FNN")) trainFNN();
        if (Inputs.NN_TYPE.equals("NEAT")) trainNEAT();
    }

    /**
     * This method trains the neural network population using a genetic algorithm.
     * It selects parent Individuals using roulette selection, removes the worst individuals from the population,
     * saves the neural network of the best individual, resets the population, and performs genetic operations
     * (crossover and mutation) to generate new Individuals for the population until the population reaches the
     * desired size.
     */
    private void trainFNN(){
        // Select parent Individuals using roulette selection
        ArrayList<Individual> parents = rouletteSelect(numParents);

        // Remove the worst individuals from the population
        population.removeAll(population.getWorst(popSize - numElitists));

        // Save the neural network of the best individual
        getBest().getNN().save();

        // Reset the population
        getPopulation().reset();

        // Generate new individuals through crossover and mutation until the population reaches the desired size
        while (getPopulation().getPopulation().size() < popSize){

            // Shuffle the list of parents to randomize the selection order
            Collections.shuffle(parents);

            // For each pair of parents, perform crossover and mutation to generate two children,
            // then add them to the population
            for (Individual child : crossOver(parents.get(0), parents.get(1))){
                gaussianMutate(child, probMutate, 0, 1, mutateScale);
                getPopulation().add(child);
            }
        }
    }

    /**
     * This method trains a neural network using the NEAT algorithm.
     */
    private void trainNEAT(){
        saveStatistics();

        // Remove the worst individuals from the population.
        population.removeAll(population.getWorst(popSize - numElitists));

        // Speciation adjusts weights
        // Perform speciation to group individuals into species based on their similarities.
        population.speciate();

        System.out.println(String.format("There are %d species", population.getSpecies().size()));
        adjustMutationRate(population.getSpecies().size());

        System.out.println(String.format("Best Neural Network has: %d Nodes, and %d Connections",
                ((NEAT)population.getFittest(1).get(0).getNN()).getNodes().size(),
                ((NEAT)population.getFittest(1).get(0).getNN()).getConnections().size()));

        // Generate new individuals until the population size is equal to the desired size.
        while (population.getPopulation().size() < popSize) {
            // Tournament selection for what specie to reproduce (rounds = 3)
            Species chosenSpecies = population.getSpecies().get(Math.max(0, rand.nextInt(population.getSpecies().size())-1));
            for (int i = 0; i < 3; i++){
                Species randomSpecies = population.getSpecies().get(Math.max(0, rand.nextInt(population.getSpecies().size())-1));
                if (chosenSpecies.getTotalFitness() < randomSpecies.getTotalFitness()) chosenSpecies = randomSpecies;
            }

            // Create a new individual by performing crossover and mutation.
            Individual child;
            if (chosenSpecies.numMembers() < 2) {
                child = NEATCrossover(chosenSpecies.get(0), population.getFittest(1).get(0)).get(0);
            }
            else{
                ArrayList<Individual> parents = chosenSpecies.getBest(2);
                child = NEATCrossover(parents.get(0), parents.get(1)).get(0);
            }
            NEATMutate(child);
            population.add(child);
        }

        population.reset();
    }

    private void saveStatistics(){
        getBest().getNN().save();

        File file = new File("src/main/resources/static/data/NEATGens.csv");
        try {
            CSVWriter writer = new CSVWriter(new FileWriter(file, true));

            String[] stats = new String[3];

            // numNodes + numConns
            stats[0] = Integer.toString(((NEAT)population.getFittest(1).get(0).getNN()).getNodes().size() +
                    ((NEAT)population.getFittest(1).get(0).getNN()).getConnections().size());
            stats[1] = Double.toString(population.getFittest(1).get(0).getFitness());
            stats[2] = Double.toString(population.getTotalFitness() / popSize);

            writer.writeNext(stats);

            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * ROULETTE WHEEL SELECTION
     * Probabilistically chooses the individuals from the population based on
     * the individual fitness over the total fitness
     *
     * Gets a random value in [0,totalFitness)
     * iterates through the population until the chosen value
     * is less than or equal to the cumulative fitness
     *
     * @param numberOfParents The number of parents to be selected
     * @return Arraylist of selected individuals
     */
    public ArrayList<Individual> rouletteSelect(int numberOfParents){
        ArrayList<Individual> selected = new ArrayList<Individual>();
        int cumulative;
        double totalFitness = population.getTotalFitness();;

        // Selects parents using roulette wheel selection method
        for (int j = 0; j < numberOfParents; j++){
            double pick = rand.nextDouble() * totalFitness;
            cumulative = 0;
            for (Individual i : population.getPopulation()){
                cumulative += i.getFitness();
                if (cumulative >= pick){
                    selected.add(i);
                    population.remove(i);
                    totalFitness -= i.getFitness();
                    break;
                }
            }
        }

        // Adds the selected parents back into the population
        population.addAll(selected);
        System.out.println("Selected Size: " + selected.size());

        // Could come up with a speciation to tell how different the parents are exactly
        // Could add up the difference from the abs val of all the weights.
        // That difference would describe how different each individual is, and
        // also help with understanding convergence and divergence a little better
        assert(!selected.get(0).equals(selected.get(1))) : "Cloned Parents = Cloned Children";

        return selected;
    }

    /**
     * Selects the best Individuals
     * Specifically it returns an ArrayList of NUM_PARENTS individuals
     * @return ArrayList of the best individuals, size = NUM_PARENTS
     */
    public ArrayList<Individual> elitistSelect(int numberOfParents){
        return population.getFittest(numberOfParents);
    }

    /**
     * Given two parents, create two new children
     */

    /**
     * Performs crossover between two parent individuals.
     * @param parent1 the first parent individual
     * @param parent2 the second parent individual
     * @return an ArrayList of individuals generated from crossover
     */
    public ArrayList<Individual> crossOver(Individual parent1, Individual parent2){
        // Randomly choose a crossover method based on a probability distribution
        double chosen = rand.nextDouble();

        if (chosen < .5){
            return uniformBinaryCrossOver(parent1, parent2);
        }
        else if (chosen < 0){
            return singlePointCrossOver(parent1, parent2);
        }
        else {
            return independentCrossover(parent1, parent2);
        }
    }

    /**
     * Splits the parent's chromosome into two sections;
     * Randomly chooses which child gets which section of each parent's chromosome
     *
     * @return The new children in an ArrayList<Individual>
     */
    private ArrayList<Individual> singlePointCrossOver(Individual parent1, Individual parent2){
        FullyConnectedNN child1NN = new FullyConnectedNN((FullyConnectedNN) parent1.getNN());
        FullyConnectedNN child2NN = new FullyConnectedNN((FullyConnectedNN) parent2.getNN());

        Individual child1 = new Individual(child1NN, Color.MAGENTA);
        Individual child2 = new Individual(child2NN, Color.MAGENTA);
        ArrayList<Individual> children = new ArrayList<Individual>();

        children.add(child1);
        children.add(child2);

        return children;
    }

    /**
     * This method performs a uniform binary crossover operation between two parents and returns
     * the new children. This operation randomly chooses which child gets which parent's respective gene.
     *
     * Randomly chooses which child gets which parent's respective gene.
     * @return The new children
     */
    private ArrayList<Individual> uniformBinaryCrossOver(Individual parent1, Individual parent2){
        FullyConnectedNN child1NN = new FullyConnectedNN((FullyConnectedNN) parent1.getNN());
        FullyConnectedNN child2NN = new FullyConnectedNN((FullyConnectedNN) parent2.getNN());

        // Layer
        // For each layer in the neural network
        for (int i = 0; i < child1NN.getNumLayers(); i++){
            // Neuron
            // For each neuron in the layer
            for (int j = 0; j < child1NN.getSizeOfLayer(i); j++){
                // Weights
                // Randomly select which parent's gene to inherit for this weight
                for (int k = 0; k < child1NN.getNodeAt(i, j).getWeights().size(); k++){

                    // Randomly select which parent's gene to inherit for this weight
                    if (rand.nextBoolean()){
                        child1NN.getNodeAt(i, j).setWeightAt(k, child2NN.getNodeAt(i, j).getWeightAt(k));
                        child2NN.getNodeAt(i, j).setWeightAt(k, child1NN.getNodeAt(i, j).getWeightAt(k));
                    }
                }

                // Randomly select which parent's gene to inherit for this neuron's bias
                if (rand.nextBoolean()){
                    child1NN.getNodeAt(i, j).setBias(child2NN.getNodeAt(i, j).getBias());
                    child2NN.getNodeAt(i, j).setBias(child1NN.getNodeAt(i, j).getBias());
                }
            }
        }

        // Create new Individual objects for the children, with the updated neural networks
        Individual child1 = new Individual(child1NN, Color.PINK);
        Individual child2 = new Individual(child2NN, Color.PINK);

        // Create an ArrayList to hold the new children and return it
        ArrayList<Individual> children = new ArrayList<Individual>();
        children.add(child1);
        children.add(child2);

        return children;
    }

    /**
     * This is custom crossover that is supposed to make smaller changes that introduce variety in weights without simply copying
     * weights from parents to children
     * @param parent1
     * @param parent2
     * @return
     */

    /**
     * Performs independent crossover between two parents by taking the average of their weights and biases,
     * then adding and subtracting a small scale amount to each weight and bias respectively to introduce variability
     * @param parent1 The first parent to cross over
     * @param parent2 The second parent to cross over
     * @return An ArrayList of the two children resulting from the crossover
     */
    public ArrayList<Individual> independentCrossover(Individual parent1, Individual parent2){
        double scaleAmount = .1;
        FullyConnectedNN child1NN = new FullyConnectedNN((FullyConnectedNN) parent1.getNN());
        FullyConnectedNN child2NN = new FullyConnectedNN((FullyConnectedNN) parent2.getNN());

        // Layer
        for (int i = 0; i < child1NN.getNumLayers(); i++){
            // Neuron
            for (int j = 0; j < child1NN.getSizeOfLayer(i); j++){
                // Weights
                for (int k = 0; k < child1NN.getNodeAt(i, j).getWeights().size(); k++){

                    // Take the average of the corresponding weight in each parent's network, add and subtract a scale
                    // amount, and assign the new weights to the children's networks
                    child1NN.getNodeAt(i, j).setWeightAt(k, (child1NN.getNodeAt(i, j).getWeightAt(k) + child2NN.getNodeAt(i, j).getWeightAt(k)) / 2 + scaleAmount);
                    child2NN.getNodeAt(i, j).setWeightAt(k, (child1NN.getNodeAt(i, j).getWeightAt(k) + child2NN.getNodeAt(i, j).getWeightAt(k)) / 2 - scaleAmount);
                }

                // Take the average of the corresponding bias in each parent's network, add and subtract a scale amount,
                // and assign the new biases to the children's networks
                child1NN.getNodeAt(i, j).setBias((child1NN.getNodeAt(i, j).getBias() + child2NN.getNodeAt(i, j).getBias()) / 2 + scaleAmount);
                child2NN.getNodeAt(i, j).setBias((child1NN.getNodeAt(i, j).getBias() + child2NN.getNodeAt(i, j).getBias()) / 2 - scaleAmount);
            }
        }

        // Create the children individuals using the newly created neural networks and assign
        // them different colors for visualization
        Individual child1 = new Individual(child1NN, Color.BLUE);
        Individual child2 = new Individual(child2NN, Color.BLUE);
        ArrayList<Individual> children = new ArrayList<Individual>();

        children.add(child1);
        children.add(child2);

        return children;
    }

    /**
     * Goes through an Individual, at each gene, if it should mutate, will add
     * a normally distributed amount around mu with standard dev sigma, and multiply that addition
     * by scale.
     */
    public void gaussianMutate(Individual ind, double probMutate, double mu, double sigma, double scale){
        FullyConnectedNN indNN = (FullyConnectedNN) ind.getNN();

        // Layer
        for (int i = 0; i < indNN.getNumLayers(); i++){
            // Neuron
            for (int j = 0; j < indNN.getSizeOfLayer(i); j++){
                // Connection
                for (int k = 0; k < indNN.getNodeAt(i,j).numWeights(); k++){
                    if (rand.nextDouble() < probMutate){
                        indNN.getNodeAt(i, j).addToWeight(k, (rand.nextGaussian() * sigma + mu) * scale);
                        ind.setColor(Color.orange);
                    }
                }
                if (rand.nextDouble() < probMutate) {
                    indNN.getNodeAt(i, j).addToBias((rand.nextGaussian() * sigma + mu) * scale);
                    ind.setColor(Color.orange);
                }
            }
        }
    }


    // ----------------------------------------------------NEAT---------------------------------------------------------------

    /**
     * Adjusts the probability of adding a new connection based on the number of species in the population.
     * @param numSpecies the number of species in the population
     */
    private void adjustMutationRate(int numSpecies){
        // Increase the probability of adding a new connection if the number of species is too low by 20%
        if (numSpecies < Inputs.DEFAULT_NUM_ELITISTS * .1) Inputs.PROB_ADD_CONNECTION *= 1.2;

        // Decrease the probability of adding a new connection if the number of species is too high by 20%
        if (numSpecies > Inputs.DEFAULT_NUM_ELITISTS * .2) Inputs.PROB_ADD_CONNECTION *= .8;
    }

    /**
     * Performs crossover between two parent individuals using the NEAT algorithm.
     *
     * @param parent1 the first parent individual
     * @param parent2 the second parent individual
     * @return an ArrayList of child individuals resulting from the crossover
     */
    public ArrayList<Individual> NEATCrossover(Individual parent1, Individual parent2){
        // Ensure that parent1 has the larger fitness
        if (parent1.getFitness() < parent2.getFitness()){
            Individual temp = parent1;
            parent1 = parent2;
            parent2 = temp;
        }

        NEAT childNN = new NEAT(Inputs.NN_STRUCTURE);

        NEAT parent1NN = (NEAT) parent1.getNN();
        NEAT parent2NN = (NEAT) parent2.getNN();

        Collection<Connection> parent1Conns = parent1NN.getConnections().values();
        Collection<Connection> parent2Conns = parent2NN.getConnections().values();

        for (Connection conn : parent1Conns){
            // If they're matching, randomly choose which parent
            if (parent2Conns.contains(conn) && rand.nextBoolean()){
                childNN.addConnection(parent2NN.getConnection(conn));
                continue;
            }
            // If they're not matching, the conn is disjoint/excess, and is inherited from the fitter parent
            childNN.addConnection(conn);
        }

        ArrayList<Individual> children = new ArrayList<Individual>();
        children.add(new Individual(childNN, Color.PINK));
        return children;
    }

    /**
     * Mutates an individual using NEAT mutation operators, including weight mutation, node mutation, and connection mutation.
     * @param ind the individual to be mutated
     */
    public void NEATMutate(Individual ind){
        NEATWeightMutate(ind, Inputs.PROB_WEIGHT_MUTATE, 0, 1, mutateScale);
        NEATNodeMutate(ind);
        NEATConnectionMutate(ind);

    }

    /**
     * Performs a weight mutation on the neural network of the given individual in a NEAT algorithm.
     * @param ind the individual to mutate
     * @param probMutate the probability that a given weight will be mutated
     * @param mu the mean of the Gaussian distribution from which the mutation value is sampled
     * @param sigma the standard deviation of the Gaussian distribution from which the mutation value is sampled
     * @param scale the scale factor for the mutation value
     */
    private void NEATWeightMutate(Individual ind, double probMutate, double mu, double sigma, double scale){
        NEAT indNN = (NEAT) ind.getNN();
        for (Node node : indNN.getNodes().values()){
            if (rand.nextDouble() < probMutate) node.addToBias((rand.nextGaussian() * sigma + mu) * scale);
        }
        for (Connection conn : indNN.getConnections().values()){
            if (rand.nextDouble() < probMutate) conn.addToWeight((rand.nextGaussian() * sigma + mu) * scale);
        }
    }

    /**
     * Performs node mutation in NEAT algorithm.
     * Selects connections to mutate based on a probability value, then adds a new node to each selected
     * connection by splitting the connection and inserting the new node between the two ends.
     * The new node is assigned a random ID and a bias value of 0.
     * @param ind the individual to mutate
     */
    private void NEATNodeMutate(Individual ind){
        NEAT indNN = (NEAT) ind.getNN();
        Collection<Connection> connections = indNN.getConnections().values();
        ArrayList<Integer> connIdsToMutate = new ArrayList<Integer>();
        for (Connection conn : connections){
            if (rand.nextDouble() < Inputs.PROB_ADD_NODE){
                connIdsToMutate.add(conn.getId());
            }
        }
        for (Integer id : connIdsToMutate){
            indNN.addNodeToConn(id);
        }
    }

    /**
     * Mutates an individual's NEAT neural network by adding a new connection.
     *
     * @param ind The individual to mutate.
     */
    private void NEATConnectionMutate(Individual ind){
        NEAT indNN = (NEAT) ind.getNN();
        Collection<Node> nodes = indNN.getNodes().values();

        for (Node fromNode : nodes){
            for (Node toNode : indNN.getLegalConnections(fromNode)){
                if (rand.nextDouble() < Inputs.PROB_ADD_CONNECTION){
                    indNN.addConnection(fromNode.getId(), toNode.getId(), rand.nextDouble(2)-1);
                }
            }
        }
    }
}
