/*
 * Created by Carter Koehn on 2023.4.12
 * Copyright © 2023 Carter Koehn. All rights reserved.
 */

package com.gaenvironmentstudy.app;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;

/**
    All this code from https://spring.io/guides/gs/uploading-files/
 */
public interface StorageService {

    void init();

    void store(MultipartFile file);

    Stream<Path> loadAll();

    Path load(String filename);

    Resource loadAsResource(String filename);

    void deleteAll();

}