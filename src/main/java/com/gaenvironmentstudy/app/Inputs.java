package com.gaenvironmentstudy.app;

import java.util.Random;

public class Inputs {
    // Individual have a max range they can detect walls
    public static final int MAX_STEPS_IN_COLLISION = 10;

    public static final Random rand = new Random(8);

    // Not final because command line needs to be able to set this value
    public static int[] NN_STRUCTURE = new int[] {9,5};
    public static String NN_TYPE = "NEAT";
    public static int NUM_GENERATIONS = 10000;

    public static final boolean SHOW_AI_VISION = true;
    public static final int FPS = 10000;
    public static final String MAP_PATH = "src/main/resources/static/images/Maps/RaceTrack2.png";
    public static final double INDIVIDUAL_ACCELERATION = .02;
    public static final int TERMINAL_VELOCITY = 7;
    public static final int MOVES_ALLOWED = 500;


    public static final int DEFAULT_POPULATION_SIZE = 200;
    public static final int DEFAULT_NUM_PARENTS = 20;
    public static final int DEFAULT_NUM_ELITISTS = 100;
    public static final double DEFAULT_MUTATION_SCALE = .2;
    public static final double DEFAULT_PROB_MUTATE = .005;

    // Not implemented Probability of mutating decaying over time
    public static final boolean DECAY = false;

    // NEAT things
    public static double PROB_WEIGHT_MUTATE = .1;
    public static double PROB_ADD_NODE = .02;
    public static double PROB_ADD_CONNECTION = .05;
    public static final int COMPATIBLITY_THRESHOLD = 3;
}

