/*
 * Created by Carter Koehn on 2023.4.12
 * Copyright © 2023 Carter Koehn. All rights reserved.
 */

package com.gaenvironmentstudy.app;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
    All this code from https://spring.io/guides/gs/uploading-files/
 */
@ConfigurationProperties("storage")
public class StorageProperties {

    /**
     * Folder location for storing files
     */
    private String location = "src/main/resources/static/images/upload-dir";

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
