/*
 * Created by Carter Koehn on 2023.4.12
 * Copyright © 2023 Carter Koehn. All rights reserved.
 */

package com.gaenvironmentstudy.app;

/**
    All this code from https://spring.io/guides/gs/uploading-files/
 */
public class StorageFileNotFoundException extends StorageException {

    public StorageFileNotFoundException(String message) {
        super(message);
    }

    public StorageFileNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}