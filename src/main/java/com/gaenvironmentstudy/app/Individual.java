package com.gaenvironmentstudy.app;
import java.awt.Color;

import FNN.*;
import NEAT.*;

/**
 * Individuals are entities that explore an environment. They belong to a population of other individuals.
 * In this implementation, a GA is used to make the individuals of a population compete against each other.
 * The result trains the NN within each individual to mimic the best representation of the fitness function
 */
public class Individual implements Comparable{
    public static final int SIZE = 10;

    private NeuralNetwork nn;
    // Distance to wall in 5 directions revolving around theta
    private double d0,d1,d2,d3,d4;
    private double velocity;
    private double theta;   // direction of travel in degrees 0 <= theta < 360
    private double x,y;

    private Color color;
    private boolean alive;
    private int numMoves = 0;
    private double fitness;

    /**
     * Complete Constructor for setting every value. Used for complete control of an Individual
     *
     * @param nn The neural network to be used ***Careful of reference issues***
     * @param velocity The starting velocity of the individual
     * @param theta The starting direction of the velocity in degrees
     * @param x The starting x position in the image
     * @param y The starting y position in the image
     * @param color The color of the individual
     */
    public Individual(NeuralNetwork nn, double velocity, double theta, double x, double y, Color color) {
        this.nn = nn;
        this.velocity = velocity;
        this.theta = theta;
        this.x = x;
        this.y = y;
        this.color = color;
        this.alive = true;
    }

    /**
     * Constructor for NN and color
     * Used for control when copying individuals carrying over just their NN and specific color
     * @param nn
     * @param color
     */
    public Individual(NeuralNetwork nn, Color color){
        this(nn, 0, 0, 0, 0, color);
    }
    public Individual(NeuralNetwork nn){
        this(nn, 0, 0, 0, 0, Color.green);
    }

    public int getx(){return (int)x;}
    public int gety() {return (int)y;}
    public NeuralNetwork getNN(){return nn;}
    public Color getColor(){return color;}
    public void setColor(Color newColor){color = newColor;}
    public double getTheta(){return theta;}
    public double getFitness(){return fitness;}
    public void setFitness(double fitness) {this.fitness = fitness;}
    public boolean isAlive() {return alive;}

    /**
     * Individuals are reused instead of creating new ones as copies
     * Reset allows for individuals to keep their NN, but is reset to the beginning
     * appropriately
     */
    public void reset(){
        color = Color.green;
        x = 0;
        y = 0;
        velocity = 0;
        theta = 0;
        alive = true;
        numMoves = 0;
        fitness = 0;
    }

    /**
     * This determines how well an individual is currently doing
     * Is called upon death to determine how well it did
     */
    private void calcFitness(){
        fitness = 10*Math.sqrt(Math.pow(x,2) + Math.pow(y,2)) - numMoves;
        if (Inputs.NN_TYPE.equals("NEAT")) fitness = 10*Math.sqrt(Math.pow(x,2) + Math.pow(y,2)) - 3*((NEAT)nn).getConnections().size();
        if (fitness < 0) { fitness = 0; }
    }

    /**
     * After this is called, the individual will stop feedforwarding and moving
     */
    private void kill() {
        calcFitness();
        alive = false;
    }

    /**
     * Sets the input of the NN
     * feedforwards through the NN, but does not return anything
     */
    public void feedForward(Screen screen){
        // double[] input = {d0, d1, d2, d3, d4, d5, d6, d7, x, y, xvel, yvel, xacc, yacc};
        if (nn.getInputSize() == 7){
            double[] input = {theta, velocity, d0, d1, d2, d3, d4};
            nn.feedForward(input);
        }
        else if (nn.getInputSize() == 9){
            double[] input = {x/screen.getWidth(), y/screen.getHeight(), theta, velocity, d0, d1, d2, d3, d4};
            nn.feedForward(input);
        }
    }

    /**
     * Uses the output vector from feedforwarding to update the positions and velocities
     * of this individual
     *
     * index 0 = Nothing
     * index 1 = Speed up
     * index 2 = Slow Down
     * index 3 = Turn Left
     * index 4 = Turn Right
     */
    public void move(Screen screen){
        int maxInd = nn.getMaxOutput();

        if (maxInd == 1){
            velocity += Inputs.INDIVIDUAL_ACCELERATION;
            velocity = Math.min(velocity, Inputs.TERMINAL_VELOCITY);
        }
        if (maxInd == 2){
            velocity -= Inputs.INDIVIDUAL_ACCELERATION;
            // -1 allows for reverse, but if it was 0, it could only head in the direction of travel
            velocity = Math.max(velocity, Inputs.TERMINAL_VELOCITY * 0);
        }
        if (maxInd == 3){
            theta -= 10;
        }
        if (maxInd == 4){
            theta += 10;
        }

        theta %= 360;
        // Move in the Direction of Theta with velocity amount
        x += velocity * Math.cos(Math.toRadians(theta));
        y += velocity * Math.sin(Math.toRadians(theta));

        if (screen.isColliding((int)x, (int)y)){
            kill();
        }
        if (++numMoves > Inputs.MOVES_ALLOWED){
            kill();
        }
        if (x <= 10 && y <= 10 && numMoves >= 100){
            kill();
        }
    }

    /**
     * Calculates the euclidean distance to each wall from given values
     * @param screen is used to ray cast until collision determined by screen
     */
    public void calcDistanceToWalls(Screen screen){
        d0 = getDistanceToWall(screen, (theta - 60));
        d1 = getDistanceToWall(screen, (theta - 30));
        d2 = getDistanceToWall(screen, (theta));
        d3 = getDistanceToWall(screen, (theta + 30));
        d4 = getDistanceToWall(screen, (theta + 60));
    }

    /**
     * Given direction, casts a ray to the nearest wall returns distance
     * @param screen provides access to the collision method
     * @param Theta The direction the ray is cast in
     * @return The euclidean length of the ray
     */
    private double getDistanceToWall(Screen screen, double Theta){
        double xinc = Math.cos(Math.toRadians(Theta)) * (SIZE - 1);
        double yinc = Math.sin(Math.toRadians(Theta)) * (SIZE - 1);

        // System.out.println(String.format("Theta: %5.2f, xinc: %3.2f, yinc: %3.2f", Theta, xinc, yinc));

        double xdist = 0;
        double ydist = 0;

        //isCollidingCircle(x, y, radius)
        int counter = 0;
        while(!screen.isColliding((int)(x + xdist), (int)(y + ydist)) && counter < Inputs.MAX_STEPS_IN_COLLISION){
            xdist += xinc;
            ydist += yinc;

            counter++;
        }

        return Math.sqrt(Math.pow(xdist, 2) + Math.pow(ydist, 2));
    }

    @Override
    public String toString(){
        return String.format("(x:%4.0f, y:%4.0f, Fitness: %5.2f)", x, y, fitness);
    }

    /**
     * Individual Equality is determined by their Neural Network equivalency
     * @param other the other object being tested for equality
     * @return boolean
     */
    @Override
    public boolean equals(Object other){
        if (other == null){
            return false;
        }
        if (other.getClass() == this.getClass()){
            Individual o = (Individual) other;
            return nn.equals(o.getNN());
        }
        return false;
    }

    /**
     * Individuals are compared via fitness values.
     * They're sorted in increasing order of fitness
     * EX:
     *      this.compareTo(other); // this.getFitness() == 100, other.getFitness() == 10;
     *      this will come second because it has higher fitness
     *
     * @param o the object to be compared.
     * @return 1,0,-1 : higher, even, lower fitness value than param o;
     */
    @Override
    public int compareTo(Object o) {
        assert(o.getClass() == this.getClass());
        Individual other = (Individual) o;

        double ret = (getFitness() - other.getFitness());

        if (ret < 0){
            return -1;
        }
        else if (ret > 0){
            return 1;
        }
        else{
            return 0;
        }
    }

    /**
     * Calculates a value that represents how similar two individuals are
     * Used to measure diversity between mass amounts of individuals
     * @param other
     * @return
     */
    public double correlation(Individual other){
        return 0;
    }
}
