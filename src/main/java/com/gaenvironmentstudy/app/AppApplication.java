package com.gaenvironmentstudy.app;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.gaenvironmentstudy.app.Runner.GA;

@SpringBootApplication
@RestController
@EnableConfigurationProperties(StorageProperties.class)
public class AppApplication {

	static Runner r = new Runner();
	public static Screen screen = new Screen(Inputs.MAP_PATH);

	/**
		Runs the spring boot backend
		Andrew
	 */
	public static void main(String[] args) {
		SpringApplication.run(AppApplication.class, args);
		r.setup();
	}

	/**
		Initializes the storage service
		https://spring.io/guides/gs/uploading-files/ 
	 */
	@Bean
	CommandLineRunner init(StorageService storageService) {
		return (args) -> {
			storageService.deleteAll();
			storageService.init();
		};
	}

	/**
		Gets the population from the backend	
		Andrew
	 */
	@GetMapping("/api/population")
	public Population getPopulation() {
		return GA.getPopulation();
	}

	/**
		Runs the genetic algorithm when button is pushed on front end
		Andrew
	 */
	@GetMapping("/run")
	public void runApp() {
		r = new Runner();
		r.setup();
		r.run();
	}

	/**
		Stops the genetic algorithm when button is pushed on front end
		Andrew
	 */
	@GetMapping("/stop")
	public void stopApp() {
		r.stopped = true;
	}
}
