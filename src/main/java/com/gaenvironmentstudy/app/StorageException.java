/*
 * Created by Carter Koehn on 2023.4.12
 * Copyright © 2023 Carter Koehn. All rights reserved.
 */

package com.gaenvironmentstudy.app;

/**
    All this code from https://spring.io/guides/gs/uploading-files/
 */
public class StorageException extends RuntimeException {

    public StorageException(String message) {
        super(message);
    }

    public StorageException(String message, Throwable cause) {
        super(message, cause);
    }
}