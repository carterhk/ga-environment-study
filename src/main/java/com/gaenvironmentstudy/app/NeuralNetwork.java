/*
 * Created by Carter Koehn on 2023.4.20
 * Copyright © 2023 Carter Koehn. All rights reserved.
 */

package com.gaenvironmentstudy.app;

import java.util.ArrayList;

public abstract class NeuralNetwork {
    public abstract ArrayList feedForward(double[] input);
    public abstract int getMaxOutput();
    public abstract void save();
    public abstract boolean equals(Object o);
    public abstract int getInputSize();
    public abstract String getType();
}
