/*
 * Created by Carter Koehn on 2023.4.22
 * Copyright © 2023 Carter Koehn. All rights reserved.
 */

package com.gaenvironmentstudy.app;

public class RunWithoutGraphics {
    public static void main(String[] args) {
        Runner trial = new Runner();
        trial.setup();
        trial.run();
    }
}
