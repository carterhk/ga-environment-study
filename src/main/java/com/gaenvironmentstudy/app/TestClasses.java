
package com.gaenvironmentstudy.app;

import FNN.FullyConnectedNN;

import java.awt.*;
import java.util.ArrayList;

public class TestClasses {
    public static void main(String[] args) {
        System.out.println("Population Fittest: " + testPopulationGetFittest());
        System.out.println("Population Worst: " + testPopulationGetWorst());
        System.out.println("GA RouletteSelection: " + testGARouletteSelection());
    }

    public static boolean testPopulationGetFittest(){
        Population pop = new Population(0);
        for (int i = 0; i < 200; i++){
            pop.add(new Individual(new FullyConnectedNN("src/main/resources/static/data/FCNN.csv"), Color.BLACK));
        }

        ArrayList<Individual> fittest = pop.getFittest(20);

        return fittest.size() == 20;
    }

    public static boolean testPopulationGetWorst(){
        Population pop = new Population(0);
        for (int i = 0; i < 200; i++){
            pop.add(new Individual(new FullyConnectedNN("src/main/resources/static/data/FCNN.csv"), Color.BLACK));
        }

        ArrayList<Individual> worst = pop.getWorst(20);

        return worst.size() == 20;
    }

    public static boolean testGARouletteSelection(){
        Population pop = new Population(0);
        GeneticAlgorithm ga = new GeneticAlgorithm();
        for (int i = 0; i < 200; i++){
            pop.add(new Individual(new FullyConnectedNN("src/main/resources/static/data/FCNN.csv"), Color.BLACK));
        }
        ga.setPopulation(pop);
        ArrayList<Individual> children = ga.rouletteSelect(20);

        return children.size() == 20;
    }

}
