/*
 * Created by Carter Koehn on 2023.4.8
 * Copyright © 2023 Carter Koehn. All rights reserved.
 */

package DataHandling;

import FNN.FullyConnectedNN;
import com.gaenvironmentstudy.app.*;
import com.opencsv.CSVWriter;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Given a directory with subdirectories of neural networks, this will iterate through each nn and each map;
 * it will record the results in a result.csv that is placed in each neural network subdirectory
 *
 * These result tables are "normalized" (divided by the result of the nn trained on that map), the columns are neural networks trained on the header of that column
 * The Rows are the map performances
 *
 */
public class GenerateResults {
    private static Population population;
    private static GeneticAlgorithm ga;

    static JFrame frame = new JFrame("Trial Populations");
    static Screen screen = new Screen("src/main/resources/static/images/Maps/RaceTrack1.png");

    private static File mapDir = new File("src/main/java/DataHandling/Maps");
    private static File parentDir = new File("src/main/java/DataHandling/200Gen2/");
    private static File nnDir;

    public static void main(String[] args) {
        for (File nnDir_ : parentDir.listFiles(File::isDirectory)) {
            if (nnDir_.getName().equals("DB_Data")){
                continue;
            }
            nnDir = nnDir_;
            // Delete contents of the testing csv
            File file = new File(nnDir.getPath() + "/results.csv");
            try {
                CSVWriter writer = new CSVWriter(new FileWriter(file));
                // Iterate over the CSV files in the subdirectory
                String[] headerInput = new String[mapDir.listFiles().length + 1];
                headerInput[0] = "Training Maps ->";
                int counter = 1;
                for (File mapFile : mapDir.listFiles((dir, name) -> name.endsWith(".png"))) {
                    headerInput[counter++] = mapFile.getName();
                }
                writer.writeNext(headerInput);

                writer.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }


            // Iterate over the CSV files in the subdirectory
            for (File mapFile : mapDir.listFiles((dir, name) -> name.endsWith(".png"))) {
                screen.setBackground(mapFile.getPath());
                setup();
                runGeneration(population, mapFile.getPath());
            }
        }
    }

    private static void customPopulation(){

        // Iterate over the CSV files in the subdirectory
        for (File nnFile : nnDir.listFiles((dir, name) -> name.endsWith(".csv"))) {
            if (nnFile.getName().equals("results.csv")){
                continue;
            }
            population.add(new Individual(new FullyConnectedNN(nnFile.getPath()), Color.GREEN));
        }

        ga.setPopulation(population);
    }

    public static void setup(){
        ga = new GeneticAlgorithm();
        population = new Population(0);
        customPopulation();

        screen.setIndividuals(population.getPopulation());

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(screen.getWidth() + 10, screen.getHeight() + 10));
        frame.setLocationRelativeTo(null);
        frame.getContentPane().add(screen);
        frame.pack();
        frame.setVisible(true);
    }

    private static void runGeneration(Population population, String map){
        long startTime = System.nanoTime();
        long targetTime = 1000 / Inputs.FPS;
        long URDTimeMilli = 0;
        long waitTime = 0;
        screen.setIndividuals(population.getPopulation());

        while (!population.allDead()){
            startTime = System.nanoTime();
            // System.out.println(GA.getPopulation().getFittest(1).get(0));
            for (Individual i : population.getPopulation()) {
                if (i.isAlive()) {
                    i.calcDistanceToWalls(screen);
                    i.feedForward(screen);
                    i.move(screen);
                }
            }

            screen.setNumAlive(population.getNumAlive());       // Soon to be set stats

            screen.repaint();
            URDTimeMilli = (System.nanoTime() - startTime) / 1000000;
            waitTime = targetTime - URDTimeMilli;
            waitTime = Math.max(waitTime, 0);
            try {
                Thread.sleep(waitTime);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        savePopStats(population, map);
        System.out.println(String.format("Best Fitness was: %5.2f", population.getFittest(1).get(0).getFitness()));
        population.reset();
    }

    private static void savePopStats(Population pop, String map){
        File file = new File(nnDir.getPath() + "/results.csv");

        try {
            CSVWriter writer = new CSVWriter(new FileWriter(file, true));

            String[] inputs = new String[pop.getPopulation().size() + 1];
            inputs[0] = new File(map).getName();
            int counter = 1;
            double trainedFitness = getTrainedMapScore(pop, map);
            for(Individual ind : pop.getPopulation()){
                inputs[counter++] = Double.toString(ind.getFitness() / trainedFitness);
            }
            writer.writeNext(inputs);

            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static double getTrainedMapScore(Population pop, String map){
        for(Individual ind : pop.getPopulation()){
            String nnPath = ((FullyConnectedNN)ind.getNN()).getFilePath();
            String shortNnPath = nnPath.substring(nnPath.lastIndexOf('\\'), nnPath.lastIndexOf('.'));
            String shortMapPath = map.substring(map.lastIndexOf('\\'), map.lastIndexOf('.'));

            if (shortNnPath.equals(shortMapPath)){
                return ind.getFitness();
            }
        }

        System.out.println("Map does not have a trained neural network on it");
        return 1.0;
    }
}
