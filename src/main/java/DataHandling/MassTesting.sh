#!/bin/bash
#
# Created by Carter Koehn on 2023.4.8
# Copyright © 2023 Carter Koehn. All rights reserved.
#

# This can't compare the generalization because trials isn't an executable, so you have
# to manually move the generated NNs to Trials in order to compare it on other maps. This
# script only generates trained neural networks limited by what we can choose in the jar file

JAR_FILE="GeneticAlgorithm.jar"
NEURAL_NETS=("{7,5}" "{7,10,5}" "{7,6,5}" "{7,7,5,5}" "{9,5}" "{9,12,5}" "{9,7,5}" "{9,12,7,5}")
MAPS_DIR="Maps/*"
NUM_GENERATIONS=100
PARENT_DIR="output/"

# Create a directory for a neural network if it doesn't exist
if [ ! -d "$PARENT_DIR" ];
then
  mkdir -p "$PARENT_DIR"
else
  echo "Need to rename or delete output/"
  exit
fi

for nn in "${NEURAL_NETS[@]}"; do
  for map in $MAPS_DIR; do
    # Create a directory for a neural network if it doesn't exist
    if [ ! -d "$PARENT_DIR/$nn" ]; then
      mkdir -p "$PARENT_DIR/$nn"
    fi

    # Create a directory for a neural network if it doesn't exist
    if [ ! -d "$PARENT_DIR/DB_Data" ]; then
      mkdir -p "$PARENT_DIR/DB_Data"
    fi

    java -jar "$JAR_FILE" "$nn" $NUM_GENERATIONS 200 40 20 .2 .002 "$map"

    #Copy the respective files to the NN dir, and change their names
    MAP_PATH=$(basename "$map")
    MAP_NAME="${MAP_PATH%.*}"
    cp "FCNN.csv" "$PARENT_DIR/$nn/$MAP_NAME.csv"
    cp "DB_Data.csv" "$PARENT_DIR/DB_Data/$nn-$MAP_NAME.csv"
    done
done
echo "Finished!"