/*
 * Created by Carter Koehn on 2023.1.15
 * Copyright © 2023 Carter Koehn. All rights reserved.
 */
package FNN;

import com.gaenvironmentstudy.app.Inputs;
import com.gaenvironmentstudy.app.NeuralNetwork;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.*;
import java.util.*;

/**
 * Fully Connected Neural Networks are standard neuron networks where each neuron connects to every neuron in the next layer
 */
public class FullyConnectedNN extends NeuralNetwork {
    private String filePath = null;
    private Random rand = Inputs.rand;
    private int numLayers;
    private ArrayList<ArrayList<Neuron>> layers;

    /**
     * Constructor: Creates a Fully Connected Neural Network
     * at its base, an NN is a list of lists of Neurons. A list of Neurons is a "layer", and
     * Each Neuron contains an array of weights that is the same size as the postceding layer.
     * The index in the weights array is the index of the respective neuron.
     * @param numWeightsPerLayers describes the shape of the NN. The size is the number of layers,
     *                            the ints within is the number of nodes per that layer
     */
    public FullyConnectedNN(int[] numWeightsPerLayers){
        assert(numWeightsPerLayers.length >= 2) : "Neural Networks must contain at least an input and output layer";

        numLayers = numWeightsPerLayers.length;
        layers = new ArrayList<ArrayList<Neuron>>();

        for (int i = 0; i < numWeightsPerLayers.length; i++) {
            layers.add(new ArrayList<Neuron>());
            for (int j = 0; j < numWeightsPerLayers[i]; j++){
                layers.get(i).add(new Neuron());
            }
        }
        // Fully connect every layer
        for (int i = 0; i < layers.size() - 1; i++){
            for (Neuron sourceNode : layers.get(i)){
                for (Neuron sinkNode : layers.get(i+1)){
                    sourceNode.addWeight(rand.nextDouble() * 2 - 1);
                }
            }
        }
    }

    /**
     * Given a nn, create a new FullyConnectedNN object that contains a copy without sharing
     * references to neurons
     * @param nn to be copied to the new FullyConnectedNN object
     */
    public FullyConnectedNN (FullyConnectedNN nn){
        numLayers = nn.layers.size();
        layers = new ArrayList<ArrayList<Neuron>>();

        for (int i = 0; i < numLayers; i++){
            layers.add(new ArrayList<Neuron>());
            for (Neuron node : nn.layers.get(i)){
                layers.get(i).add(new Neuron(node.getValue(), node.getBias()));
            }
        }
        // Fully connect every layer
        for (int i = 0; i < layers.size() - 1; i++){
            for (int j = 0; j < getSizeOfLayer(i); j++){
                getNodeAt(i,j).setWeights(nn.getNodeAt(i,j).getWeights());
            }
        }
    }

    /**
     * Import Constructor
     * Given a specified filepath to a saved FCNN, this will load the weights specified
     * @param filePath the FCNN csv file to be imported
     */
    public FullyConnectedNN (String filePath){
        this.filePath = filePath;
        layers = new ArrayList<ArrayList<Neuron>>();
        try {
            CSVReader csvReader = new CSVReader(new FileReader(filePath));
            String[] line = null;

            ArrayList<Neuron> layer = new ArrayList<Neuron>();
            while ((line = csvReader.readNext()) != null){
                if (line[0].equals("#")){
                    continue;
                }
                if (line[0].equals("next")){
                    layers.add(layer);
                    numLayers++;
                    layer = new ArrayList<Neuron>();
                    continue;
                }

                ArrayList<Double> weights = new ArrayList<Double>();
                for (int i = 0; i < line.length - 2; i++){
                    weights.add(Double.parseDouble(line[i]));
                }

                Neuron newNode = new Neuron(weights, Double.parseDouble(line[line.length-2]), Double.parseDouble(line[line.length-1]));
                layer.add(newNode);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getFilePath() {
        return filePath;
    }
    public ArrayList<ArrayList<Neuron>> getLayers() {
        return layers;
    }
    public Neuron getNodeAt(int layerInd, int nodeInd){
        return layers.get(layerInd).get(nodeInd);
    }
    public int getNumLayers() {
        return layers.size();
    }
    public int getInputSize() {
        return layers.get(0).size();
    }
    public int getSizeOfLayer(int layerInd) {
        return layers.get(layerInd).size();
    }
    public int getMaxOutput(){
        return layers.get(layers.size()-1).indexOf(Collections.max(layers.get(layers.size()-1)));
    }
    public String getType(){
        return "FNN";
    }
    public int[] getStructure(){
        int[] numWeightsPerlayer = new int[numLayers];
        for (int i = 0; i < numLayers; i++){
            numWeightsPerlayer[i] = getSizeOfLayer(i);
        }
        return numWeightsPerlayer;
    }

    /**
     * Iterates through each layer assigning the next layer the correct value
     * depending on the current neuron values and weights
     *
     * @return an arraylist of the output Neurons with correct weights
     */
    public ArrayList<Neuron> feedForward(double[] input){
        assert(input.length == layers.get(0).size()) : "The attempted input must match the size of the input layer";
        for (int i = 0; i < input.length; i++){
            getNodeAt(0,i).setValue(input[i]);
        }

        for (int i = 0; i < layers.size() - 1; i++){
            for (Neuron node : layers.get(i)){
                for (int nextNodeInd = 0; nextNodeInd < getSizeOfLayer(i + 1); nextNodeInd++){
                    if (getNodeAt(i + 1, nextNodeInd).isActivated()){
                        getNodeAt(i + 1, nextNodeInd).reset();
                    }
                    getNodeAt(i + 1, nextNodeInd).addToValue(node.getWeightAt(nextNodeInd) * node.getValue());
                }
            }

            for (Neuron node : layers.get(i + 1)) {
                node.addToValue(node.getBias());
                node.activate();
            }
        }

        return layers.get(layers.size()-1);
    }

    /**
     * Iterates backwards through the neural network adjusting the weights accordingly
     */
    public void backPropogate(){
        throw new RuntimeException("Not implemented yet");
    }

    /**
     * Calculates the error between the current value and the expected result
     */
    private void error() {

    }

    /**
     * Overwrites the weights, values and biases to FCNN.csv
     */
    public void save(){
        File file = new File("src/main/resources/static/data/FCNN.csv");
        try {
            CSVWriter writer = new CSVWriter(new FileWriter(file));

            for(ArrayList<Neuron> layer : layers){
                for(Neuron node: layer){
                    String[] stringWeights = new String[node.getWeights().size() + 2];
                    for(int i = 0; i < node.getWeights().size(); i++){
                        stringWeights[i] = Double.toString(node.getWeightAt(i));
                    }
                    stringWeights[stringWeights.length-2] = Double.toString(node.getBias());
                    stringWeights[stringWeights.length-1] = Double.toString(node.getValue());
                    writer.writeNext(stringWeights);
                }
                writer.writeNext(new String[] {"next"});
            }

            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Makes a string that shows the Structure of the neural network
     * Each line is a layer with the respective neurons
     * @return the String representation of a FCNN
     */
    @Override
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(String.format("\nFullyConnected Network with%2d layers", numLayers));
        for (ArrayList<Neuron> layer : layers){
            s.append("\n");
            for (Neuron node : layer){
                s.append(node.toString() + ", ");
            }
        }
        return s.toString();
    }

    /**
     * Equivalence is checked through equivalence of every single node
     * @param o the fully connected network being compared
     * @return boolean for equivalence
     */
    @Override
    public boolean equals(Object o){
        if (o == null){
            return false;
        }
        else if (o.getClass() != this.getClass()){
            return false;
        }
        else {
            FullyConnectedNN other = (FullyConnectedNN) o;
            if (numLayers != other.getNumLayers()) return false;

            for (int i = 0; i < layers.size(); i++){
                if (getSizeOfLayer(i) != other.getSizeOfLayer(i)) return false;
                for (int j = 0; j < layers.get(i).size(); j++){
                    if (!getNodeAt(i,j).equals(other.getNodeAt(i,j))) {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}

