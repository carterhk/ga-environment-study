/*
 * Created by Carter Koehn on 2023.1.15
 * Copyright © 2023 Carter Koehn. All rights reserved.
 */

package FNN;

public class testFullyConnectedNN {
    public static void main(String[] args) {
        System.out.println("Equals: " + testEquals());
        System.out.println("Save: " + testSave());
        System.out.println("Load: " + testLoad());
        testingWhatever();
    }

    /**
     * Tests the copy constructor and the equals method
     * @return Equality of copy
     */
    public static boolean testEquals(){
        int[] entry = {5,7,5};
        FullyConnectedNN testNN = new FullyConnectedNN(entry);
        FullyConnectedNN copy = new FullyConnectedNN(testNN);

        return testNN.equals(copy);
    }

    /**
     * Runs the save method on a new fcnn
     * @return true
     */
    public static boolean testSave(){
        int[] entry = {5,7,5};

        FullyConnectedNN testNN = new FullyConnectedNN(entry);
        testNN.feedForward(new double[] {0,0,0,0,0});

        testNN.save();

        return true;
    }

    public static boolean testLoad(){
        int[] entry = {7,5};
        FullyConnectedNN testNN = new FullyConnectedNN(entry);
        testNN.feedForward(new double[] {0,0,0,0,0});
        testNN.save();

        FullyConnectedNN copy = new FullyConnectedNN("src/main/resources/static/data/FCNN.csv");
        FullyConnectedNN copy2 = new FullyConnectedNN("src/main/resources/static/data/FCNN.csv");
        // System.out.println(testNN);
        // System.out.println(copy);
        // System.out.println(testNN.equals(copy));
        // System.out.println(testNN.equals(copy2));
        // System.out.println(copy2.equals(copy));
        return testNN.equals(copy);
    }

    public static void testingWhatever(){
        int[] entry = {7,5};
        FullyConnectedNN testNN = new FullyConnectedNN(entry);
        System.out.println(testNN);
    }
}
