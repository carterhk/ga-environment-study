/*
 * Created by Carter Koehn on 2023.1.15
 * Copyright © 2023 Carter Koehn. All rights reserved.
 */
package FNN;

import com.gaenvironmentstudy.app.Inputs;

import java.util.ArrayList;
import java.util.Random;

/**
 * Neurons are part of neural networks. They are supposed to make the structure simpler in theory.
 * They are used to control the relationship between each other
 */
public class Neuron implements Comparable{
    private Random rand = Inputs.rand;
    private ArrayList<Double> weights;
    private double value;
    private double bias;
    private boolean activated;

    /**
     * Default constructor
     * Bias is set to a random double [0.0, 1.0)
     */
    public Neuron() {
        weights = new ArrayList<Double>();
        activated = false;
        bias = rand.nextDouble();
    }

    /**
     * Constructor
     * Sets initial values to params
     * @param value Initial value
     * @param bias Initial bias
     */
    public Neuron(double value, double bias){
        weights = new ArrayList<Double>();
        activated = false;
        this.value = value;
        this.bias = bias;
    }

    /**
     * Copy Constructor
     * Copies the given weights
     * @param weights to be used
     */
    public Neuron(ArrayList<Double> weights, double bias, double value){
        this.weights = weights;
        this.bias = bias;
        this.value = value;
        activated = false;
    }

    public ArrayList<Double> getWeights() {
        return weights;
    }
    public int numWeights(){
        return weights.size();
    }
    public double getWeightAt(int index) {
        return weights.get(index);
    }
    public void addToWeight(int index, Double weight){
        weights.set(index, weights.get(index) + weight);
    }
    public void addWeight(Double weight){
        weights.add(weight);
    }
    public void setWeights(ArrayList<Double> newWeights){
        // This MUST add them in the same order
        weights.addAll(newWeights);
    }
    public void setWeightAt(int index, double value) {
        weights.set(index, value);
    }
    public double getValue() {
        return value;
    }
    public void setValue(double val) {
        this.value = val;
    }
    public void addToValue(double val){
        value += val;
    }
    public double getBias() {
        return bias;
    }
    public void setBias(double bias){
        this.bias = bias;
    }
    public void addToBias(double value){
        bias += value;
    }
    public boolean isActivated(){
        return activated;
    }
    public boolean hasConnections(){
        return weights.size() != 0;
    }

    /**
     * Activation for neurons adds the bias to the value and then performs
     * an activation function on the node
     */
    public void activate(){
        value += bias;
        value = relu(value);
        activated = true;
    }

    private double sigmoid(double value){
        return 1 / (1 + Math.exp(value * -1));
    }

    private double relu(double in){
        return Math.max(in, 0);
    }

    /**
     * Value is set to 0, is no longer activated
     */
    public void reset(){
        value = 0.0;
        activated = false;
    }

    /**
     * Equality is based around every weight being equivalent
     * @param o Other object being compared
     * @return boolean representing equality
     */
    @Override
    public boolean equals(Object o){
        if (o == null){
            return false;
        }
        else if (o.getClass() != this.getClass()){
            return false;
        }
        else {
            Neuron other = (Neuron) o;
            if (weights.size() != other.getWeights().size()) return false;
            for (int i = 0; i < weights.size(); i++){
                if (weights.get(i) != other.getWeightAt(i)) return false;
            }
            if (value != other.getValue()) return false;
            if (bias != other.getBias()) return false;
            return true;
        }

    }

    /**
     * Creates a String that enumerates details about the neuron
     * @return a string representation of a neuron
     */
    @Override
    public String toString(){
        StringBuilder s = new StringBuilder();

        s.append("{");
        s.append(String.format("%2.2f, %b, %d, weights:(", value, activated, weights.size()));
        // This will print the value of each weight within the neuron
//         for (Double weight : weights){
//             s.append(String.format(", %2.2f", weight));
//         }

        s.append(")}");
        return s.toString();
    }

    /**
     * Neurons are ordered by increasing value
     * @param o the object to be compared.
     * @return -1, 0, 1 : smaller value, even value, higher value
     */
    @Override
    public int compareTo(Object o) {
        assert(o != null);
        assert(o.getClass().equals(this.getClass()));
        Neuron other = (Neuron) o;

        double ret = value - other.getValue();

        if (ret < 0) return -1;
        else if (ret > 0) return 1;
        else return 0;
    }
}

